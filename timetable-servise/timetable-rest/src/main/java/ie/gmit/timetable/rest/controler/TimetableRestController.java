package ie.gmit.timetable.rest.controler;


import ie.gmit.timetable.rest.models.*;
import ie.gmit.timetable.rest.models.Module;
import ie.gmit.timetable.rest.repository.TimetableRepository;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/tt")
public class TimetableRestController {
    //    @Autowired
    private final TimetableRepository timetableRepository;

    public TimetableRestController() {
        timetableRepository = new TimetableRepository();
    }

    /**
     * Get available departments
     * @return
     */
    @GetMapping("/departments")
    private Flux<Department> getDepartments() {
        var departmentProto = ie.gmit.proto.Department.newBuilder()
                .setCampus(ie.gmit.proto.Department.Campus.GALWAY)
                .setDepartmentType(ie.gmit.proto.Department.DepartmentType.SCIENCE)
                .setName("some-random-department-name")
                .build();

        return Flux.just(Department.of(departmentProto));
    }

    /**
     * Get available courses for a given department
     * @param department
     * @return
     */
    @GetMapping("/programs")
    private Flux<Course> getPrograms(@RequestBody Department department) {
        log.info("got department: {}", department);

        var courseProto = ie.gmit.proto.Course.newBuilder()
                .setCourseCode("some-course-code")
                .setName("name-course")
                .setYear(0)
                .setSemester(1)
                .build();

        return Flux.just(Course.of(courseProto));
    }

    /**
     * Get weeks timetable for a given course
     * @param course
     * @return
     */
    @GetMapping("/timetables/course")
    private Flux<Lecture> getTimetable(@RequestBody Course course) {
        log.info("got course: {}", course);

        var roomProto = ie.gmit.proto.Room.newBuilder()
                .setNumber(100)
                .setDescription("Nice room")
                .build();

        var moduleProto = ie.gmit.proto.Module.newBuilder()
                .setCourseCode("course-code-here")
                .setModuleName("moduleName")
                .setModuleType(ie.gmit.proto.ModuleType.LECTURE)
                .setWeeks("1-13")
                .build();

        var lecturerProto = ie.gmit.proto.Lecturer.newBuilder()
                .setName("Jhon")
                .setSurname("Doe")
                .setEmail("jhon.doe.gmit.ie")
                .build();

        var lectureProto = ie.gmit.proto.Lecture.newBuilder()
                .setRoom(roomProto)
                .setStartTime(9)
                .setDuration(2)
                .setModule(moduleProto)
                .setStaff(lecturerProto)
                .setWeekDay(ie.gmit.proto.WeekDay.TUESDAY)
                .build();

        return Flux.just(Lecture.of(lectureProto));
    }

    /**
     * Get timetable for a specific day for a specific course
     * @param weekDay
     * @param course
     * @return
     */
    @GetMapping("/timetables/day")
    private Flux<Lecture> getDayTimetable(
            @PathVariable ie.gmit.proto.WeekDay weekDay,
            @PathVariable Course course
    ) {
        log.info("got: weekday: {}, course: {}", weekDay, course);
        var lectureProto = getProtoLecture("mod-1");
        var lectureProto2 = getProtoLecture("mod-2");
        var lectureProto3 = getProtoLecture("mod-3");

        return Flux.just(
                Lecture.of((lectureProto)), Lecture.of((lectureProto2)),
                Lecture.of((lectureProto3))
        );
    }

    /**
     * Get timetable for a given room
     * @param room
     * @return
     */
    @GetMapping("/timetables/room")
    private Flux<Module> getRoomTimetable(Room room){
        log.info("got: room: {}", room);

        return Flux.just();
    }

    /**
     * Get all modules for a given course
     * @param course
     * @return
     */
    @GetMapping("/modules")
    private Flux<Module> getModules(Course course){
        log.info("got: course: {}", course);


        return Flux.just();
    }

    /**
     * Get all staff for a given course
     * @param course
     * @return
     */
    @GetMapping("/staff")
    private Flux<Lecturer> getCourseStaff(Course course){
        log.info("got: course: {}", course);

        return Flux.just();
    }

    /**
     * Get next lecture for a given course
     * @param course
     * @return
     */
    @GetMapping("/modules/course")
    private Mono<Lecture> getNextLecture(Course course){
        log.info("got: course: {}", course);

        return Mono.just(null);
    }

    /*
    Tet endpoints
     */

    @GetMapping("/{id}")
    private Mono<Lecture> getNextLecture(@PathVariable String id) {
        var lectureProto = getProtoLecture("mod-1");
        return Mono.just(Lecture.of(lectureProto));
    }

    @GetMapping("/list")
    private Flux<Lecture> getAllLectures() {
        var lectureProto = getProtoLecture("mod-1");
        var lectureProto2 = getProtoLecture("mod-2");
        var lectureProto3 = getProtoLecture("mod-3");

        return Flux.just(
                Lecture.of((lectureProto)), Lecture.of((lectureProto2)),
                Lecture.of((lectureProto3))
        );
    }

    @GetMapping("/block/{id}")
    private Lecture getNextLectureBlock(@PathVariable String id) {
        return Lecture.of((getProtoLecture("bloc-1")));
    }

    @GetMapping("/block/list")
    private List<Lecture> getAllLecturesBlock() {
        return List.of(
                Lecture.of((getProtoLecture("bloc-1"))),
                Lecture.of((getProtoLecture("bloc-2")))
        );
    }

    private ie.gmit.proto.Lecture getProtoLecture(String moduleName) {
        var moduleProto = ie.gmit.proto.Module.newBuilder()
                .setCourseCode("course-code-here")
                .setModuleName(moduleName)
                .setModuleType(ie.gmit.proto.ModuleType.LECTURE)
                .setWeeks("1-13")
                .build();

        var lecturerProto = ie.gmit.proto.Lecturer.newBuilder()
                .setName("Jhon")
                .setSurname("Doe")
                .setEmail("jhon.doe.gmit.ie")
                .build();

        return ie.gmit.proto.Lecture.newBuilder()
                .setRoom(ie.gmit.proto.Room.newBuilder().setNumber(100).setDescription("Nice room").build())
                .setStartTime(9)
                .setDuration(1)
                .setModule(moduleProto)
                .setStaff(lecturerProto)
                .setWeekDay(ie.gmit.proto.WeekDay.FRIDAY)
                .build();
    }

}
