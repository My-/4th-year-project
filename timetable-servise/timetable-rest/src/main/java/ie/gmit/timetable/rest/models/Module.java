package ie.gmit.timetable.rest.models;

import ie.gmit.proto.ModuleType;
import lombok.Data;

@Data
public class Module {
    private String courseCode;
    private String moduleName;
    private String weeks;
    private ModuleType moduleType;

    private Module(ie.gmit.proto.Module moduleProto){
        this.courseCode = moduleProto.getCourseCode();
        this.moduleName = moduleProto.getModuleName();
        this.weeks = moduleProto.getWeeks();
        this.moduleType = moduleProto.getModuleType();
    }

    public static Module of(ie.gmit.proto.Module moduleProto){
        return new Module(moduleProto);
    }
}
