package ie.gmit.timetable.rest.models;



import ie.gmit.proto.ModuleType;
import ie.gmit.proto.WeekDay;
import lombok.Data;

@Data
public class Lecture {

    private Room room;
    private int startTime;
    private int duration;
    private Module module;
    private Lecturer staff;
    private WeekDay weekDay;


    private Lecture(ie.gmit.proto.Lecture lectureProto){
        this.room = Room.of(lectureProto.getRoom());
        this.duration = lectureProto.getDuration();
        this.startTime = lectureProto.getStartTime();
        this.module = Module.of(lectureProto.getModule());
        this.staff = Lecturer.of(lectureProto.getStaff());
        this.weekDay = lectureProto.getWeekDay();
    }

    public static Lecture of(ie.gmit.proto.Lecture lectureProto){
        return new Lecture(lectureProto);
    }

    public static void main(String[] args) {
        var me = new Lecture(getProtoLecture("random crap"));
//        me
    }

    public static ie.gmit.proto.Lecture getProtoLecture(String moduleName) {
        var moduleProto = ie.gmit.proto.Module.newBuilder()
                .setCourseCode("course-code-here")
                .setModuleName("moduleName")
                .setModuleType(ModuleType.LECTURE)
                .setWeeks("1-13")
                .build();

        var lecturerProto = ie.gmit.proto.Lecturer.newBuilder()
                .setName("Jhon")
                .setSurname("Doe")
                .setEmail("jhon.doe.gmit.ie")
                .build();

        return ie.gmit.proto.Lecture.newBuilder()
                .setRoom(ie.gmit.proto.Room.newBuilder().setNumber(100).setDescription("Nice room").build())
                .setStartTime(9)
                .setDuration(1)
                .setModule(moduleProto)
                .setStaff(lecturerProto)
                .setWeekDay(WeekDay.FRIDAY)
                .build();
    }


}
