package ie.gmit.timetable.rest.models;

import lombok.Data;

@Data
public class Course {
    private String courseCode;
    private String name;
    private int year;
    private int semester;

    private Course(ie.gmit.proto.Course courseProto){
        this.courseCode = courseProto.getCourseCode();
        this.name = courseProto.getName();
        this.year = courseProto.getYear();
        this.semester = courseProto.getSemester();
    }

    public static Course of(ie.gmit.proto.Course courseProto){
        return new Course(courseProto);
    }
}


