package ie.gmit.timetable.rest.models;

import lombok.Data;

@Data
public class Lecturer {
    private String name;
    private String surname;
    private String email;

    private Lecturer(ie.gmit.proto.Lecturer lecturerProto) {
        this.name = lecturerProto.getName();
        this.surname = lecturerProto.getSurname();
        this.email = lecturerProto.getEmail();
    }

    public static Lecturer of(ie.gmit.proto.Lecturer lecturerProto){
        return new Lecturer(lecturerProto);
    }

}
