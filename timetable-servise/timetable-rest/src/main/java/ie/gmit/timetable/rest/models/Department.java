package ie.gmit.timetable.rest.models;

import lombok.Data;

@Data
public class Department {
    private ie.gmit.proto.Department.Campus campus;
    private ie.gmit.proto.Department.DepartmentType departmentType;
    private String name;

    private Department(ie.gmit.proto.Department departmentProto) {
        this.campus = departmentProto.getCampus();
        this.departmentType = departmentProto.getDepartmentType();
        this.name = departmentProto.getName();
    }

    public static Department of(ie.gmit.proto.Department departmentProto){
        return new Department(departmentProto);
    }
}


