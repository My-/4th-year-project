package ie.gmit.timetable.rest.models;

import lombok.Data;

@Data
public class Room {
    private int number;
    private String description;

    private Room(ie.gmit.proto.Room roomProto){
        this.number = roomProto.getNumber();
        this.description = roomProto.getDescription();
    }

    public static Room of(ie.gmit.proto.Room roomProto) {
        return new Room(roomProto);
    }
}
