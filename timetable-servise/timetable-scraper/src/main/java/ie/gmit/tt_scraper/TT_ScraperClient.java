package ie.gmit.tt_scraper;

import com.google.protobuf.Empty;

import ie.gmit.proto.Course;
import ie.gmit.proto.Department;
import ie.gmit.proto.Lecture;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

import java.util.concurrent.TimeUnit;

@Slf4j
public class TT_ScraperClient {
    private final ManagedChannel channel;
    private final ReactorTimetableScraperGrpc.ReactorTimetableScraperStub scraperReactorStub;

    /**
     * Constructor
     *
     * @param host gRPC server host address
     * @param port gRPC server port number
     */
    public TT_ScraperClient(String host, int port){
        this.channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext()
                .build()
                ;
        this.scraperReactorStub = ReactorTimetableScraperGrpc.newReactorStub(channel);
    }

    /**
     * Factory method to create {@link TT_ScraperClient}
     * @param host gRPC server host address
     * @param port gRPC server port number
     * @return New instance of {@link TT_ScraperClient}
     */
    public static TT_ScraperClient of(String host, int port){
        return new TT_ScraperClient(host, port);
    }

    /**
     * Shut down client (close)
     * @throws InterruptedException
     */
    public void shutdown() throws InterruptedException {
        channel.shutdown()
                .awaitTermination(5, TimeUnit.SECONDS);
    }

    /**
     * Get {@link Department}'s from gRPC scraper server.
     * @return reactive stream of {@link Department}.
     */
    public Flux<Department> scrapeDepartments(){
        return scraperReactorStub.scrapeDepartments(Empty.getDefaultInstance());
    }

    /**
     * Get {@link Course}'s from gRPC scraper server.
     * @param department of which {@link Course}'s we want to get
     * @return reactive stream of {@link Course}.
     */
    public Flux<Course> scrapePrograms(Department department){
        return scraperReactorStub.scrapePrograms(department);
    }

    /**
     * Get {@link Lecture}'s from gRPC scraper server.
     * @param course of which {@link Lecture}'s we want to get
     * @return reactive stream of {@link Lecture}.
     */
    public Flux<Lecture> scrapeTimetable(Course course){
        return scraperReactorStub.scrapeTimetable(course);
    }
}
