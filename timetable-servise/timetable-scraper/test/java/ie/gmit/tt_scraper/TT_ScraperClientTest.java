package ie.gmit.tt_scraper;

import ie.gmit.proto.Course;
import ie.gmit.proto.Department;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TT_ScraperClientTest {

    private TT_ScraperClient scraperClient;

    @BeforeEach
    void setUp() {
        this.scraperClient = new TT_ScraperClient("localhost", 50051);
    }

    @SneakyThrows
    @AfterEach
    void tearDown() {

        this.scraperClient.shutdown();
    }

    @Test
    void scrapeDepartments() {
        scraperClient.scrapeDepartments()
                .doOnNext(System.out::println)
                .count()
                .doOnNext(n -> System.out.println("total: "+ n))
                .block()
                ;

    }

    @Test
    void scrapePrograms() {
        var department = Department.newBuilder()
                .setType(Department.Type.SCIENCE)
                .setCampus(Department.Campus.GALWAY)
                .setName("Dept of Computer Science & Applied Physics")
//                .setName("Dept. of Business and Accounting")
//                .setName("Mayo Campus")
                .build()
                ;

        scraperClient.scrapePrograms(department)
                .doOnNext(System.out::println)
                .count()
                .doOnNext(n -> System.out.println("total: "+ n))
                .block()
                ;
    }

    @Test
    void scrapeTimetable() {
        var course = Course.newBuilder()
                .setCourseCode("G-KSOAG4")
                .setName("BSc in Computing in Software Development")
                .setYear(4)
                .setSemester(8)
                .setNotes("G-KSOAG4 BSc in Computing in Software Development Yr 4 Sem 8")
                .build()
                ;

        scraperClient.scrapeTimetable(course)
                .doOnNext(System.out::println)
                .count()
                .doOnNext(n -> System.out.println("total: "+ n))
                .block()
                ;
    }
}