#! /bin/bash

docker run -d --rm -it \
--name tt-postgres \
-v ~/GMIT/MainProject/4th-year-project/timetable-servise/postgres/data:/var/lib/postgresql/data \
-p 5432:5432 \
-e POSTGRES_USER=postgres \
-e POSTGRES_PASSWORD=postgres \
postgres:10-alpine

