const {Builder, By, Key, until} = require('selenium-webdriver')
const chrome = require('selenium-webdriver/chrome')
const firefox = require('selenium-webdriver/firefox')
const chromedriver = require('chromedriver')          // https://stackoverflow.com/a/43816183/5322506


// // gRPC
// const PROTO_PATH = __dirname + './../timetable_scraper.proto'
// const grpc = require('grpc')
// const protoLoader = require('@grpc/proto-loader')
// const packageDefinition = protoLoader.loadSync(
//     PROTO_PATH,
//     {keepCase: true,
//         longs: String,
//         enums: String,
//         defaults: true,
//         oneofs: true
//     });
// const timetable_scraper_proto = grpc.loadPackageDefinition(packageDefinition).timetable_scraper;

// debug logger
const debug = require('debug')('selenium-scraper-debug')    // to enable: DEBUG=selenium-scraper-namespace
const name = 'Selenium-scraper'
debug('booting %s', name)


const WEB_ADDRESS = 'https://timetable.gmit.ie'

const DEPARTMENTS_SELECTOR = '#dlFilter'
const PROGRAM_SELECTOR = '#dlObject'
const TT_LAYOUT_SELECTOR = '#dlType'

const BACK_TO_TIMETABLE_SELECTION_SELECTOR = '#bNextWeek'
const BACK_TO_TIMETABLE_SELECTION_FROM_LIST_SELECTOR = 'body > table.footer-border-args > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(1) > span > table > tbody > tr > td:nth-child(1) > font > span.footer-4-0-4 > a'

/**
 * Waits for a given `timeout` for given `selector`.
 *
 * @param driver browser driver
 * @param selector Css selector
 * @param timeout time wait for element to appear.
 * @returns {Promise<void>}
 */
async function findCss({driver, selector, timeout = 1000}) {
    return driver.wait(until.elementLocated(By.css(selector)), timeout)
}

/**
 * Waits for a given `timeout` for given `selector` and
 * then clicks it.
 *
 * @param driver browser driver
 * @param selector Css selector
 * @param timeout time wait for element to appear.
 * @returns {Promise<void>}
 */
async function clickCss({driver, selector, timeout = 1000}) {
    await driver.wait(until.elementLocated(By.css(selector)), timeout).click()
}

/**
 * Goes to timetable selection page
 *
 * @param driver WebDriver
 * @returns {Promise<void>}
 */
async function timetableSelectionPage({driver}) {
    const tableText = await findCss({selector: 'li', driver})
    // log
    debug(`Timetable for: ${await tableText.getText()}`)
    // Go to recant timetables
    await tableText.findElement(By.css('li:nth-child(1) > a:nth-child(1)')).click()
}

/**
 * Selects Department from drop down menu.
 * @param driver
 * @param departmentsSelector
 * @param department
 * @returns {Promise<void>}
 */
async function selectDepartment({driver, departmentsSelector, department}) {
    // select drop down "select department"
    let departments = await driver.wait(until.elementLocated(By.css(departmentsSelector)), 1000)
    departments.click()
    // selector for a given department
    const selector = `//*[@id="${departmentsSelector.slice(1)}"]/option[contains(text(), "${department}")]`
    // click given department
    await driver.findElement(By.xpath(selector)).click()
}

async function getDepartmentsList({driver, departmentsSelector}) {
    let departments = await findCss({driver, selector: departmentsSelector})
    const departmentsList = await departments.findElements(By.css('option'))

    const dep = []

    for (let e of departmentsList) {
        dep.push(await e.getText())
    }
    return dep
}

/**
 * Select program from a "Select Program" drop down list
 * @param driver
 * @param program we want to select (ID )
 * @param semester
 * @param programsSelector
 * @returns {Promise<void>}
 */
async function selectProgram({driver, program, semester, programsSelector}) {
    // find "Select Program" drop down
    const programsDropDown = await driver.findElement(By.css(programsSelector))
    // selector for: starts same as given program and contains given semester value after string "Sem"
    const selector = `//*[@id="${programsSelector.slice(1)}"]/option[starts-with(text(), "${program}") and contains(substring-after(text(), "Sem"), ${semester})]`
    await programsDropDown.findElement(By.xpath(selector)).click()

    debug(`Program selected: ${await programsDropDown.findElement(By.xpath(selector)).getText()}`)
}

async function selectProgramFallBack({driver, program, programsSelector}) {
    // find "Select Program" drop down
    const programsDropDown = await driver.findElement(By.css(programsSelector))
    // selector for: starts same as given program and contains given semester value after string "Sem"
    const selector = `//*[@id="${programsSelector.slice(1)}"]/option[contains(text(), "${program}")]`
    await programsDropDown.findElement(By.xpath(selector)).click()

    debug(`Program selected fallback: ${await programsDropDown.findElement(By.xpath(selector)).getText()}`)
}

async function getProgramsList({driver, programsSelector}) {
    let programsList = await findCss({driver, selector: programsSelector})
    programsList = await programsList.findElements(By.css('option'))
    const programs = []

    for (let e of programsList) {
        programs.push(await e.getText())
    }

    return programs
}

async function changeTimetableLayoutToList({driver, layoutSelector}) {
    const layoutDropDown = await driver.findElement(By.css(layoutSelector))
    const selector = `${layoutSelector} > option:nth-child(2)`
    await layoutDropDown.findElement(By.css(selector)).click()
}

/**
 * Helper function to take array of promises (holding timetable
 * data for a single day) and convert it to array of objects
 * holding similar data.
 *
 * @param arr Promise of html elements
 * @returns {Promise<[]>}
 */
const toModuleList = async (arr) => { // <--- another way to declare function (declaration order maters)
    const data = []

    for (let el of arr) {
        // each table data tag (`td`) has some data about module
        const module = await el.findElements(By.css('tr > td'))
        // first `td` contains module (activity) name
        let activity = await module[0].getText()
        // split on the space for further work
        activity = activity.split(' ')
        // first element is course code
        const courseCode = activity[0]
        // take last element
        let moduleType = activity.pop()

        // getting `type` (from: "part_of_mogule_name/L" )
        if (moduleType.toUpperCase().slice(-1) === 'L') {
            const stArr = moduleType.split('/')
            moduleType = stArr.pop()
            activity.push(stArr)
        } else {     // else "Gr" is last in activity
            activity.pop()
        }

        // remove first element from array (activityCode)
        activity.shift()
        // join array to get a module name
        const moduleName = activity.join(' ')
        // crate object and push to array
        data.push({
            courseCode,
            moduleName,
            moduleType,
            startTime: await module[2].getText(),
            duration: await module[4].getText(),
            weeks: await module[5].getText(),
            room: await module[6].getText(),
            staff: await module[7].getText(),
        })  // ToDo: use schema for object creation
    }
    // remove first object as it is table header
    data.shift()
    return data
}

/**
 * This function scrapes departments
 * @param driver
 * @returns {Promise<[]>} list of departments
 */
async function departmentsScraper({driver}) {
    // go to..
    await timetableSelectionPage({driver})
    // Select "Programmes" menu option
    await clickCss({selector: '#LinkBtn_programmesofstudy', driver})
    // Get list of departments
    let dep = await getDepartmentsList({driver, departmentsSelector: DEPARTMENTS_SELECTOR})

    return dep.slice(1)
}

/**
 * This function returns a function which scrapes programs (courses)
 * Returned function should be passed to `scrape({scraper})`
 * (higher order function) as parameter. One of benefits of
 * Functional Programing paradigm.
 * @param department we want to get programs (courses) from.
 * @returns {function({driver: *}): []} function which scrapes programs
 * (courses) from given department.
 */
function getProgramsScraper({department}) {
    /**
     * Scraper function
     */
    return async ({driver}) => {
        // go to..
        await timetableSelectionPage({driver})
        // Select "Programmes" menu option
        await clickCss({selector: '#LinkBtn_programmesofstudy', driver})
        // Select "Department"
        await selectDepartment({department, departmentsSelector: DEPARTMENTS_SELECTOR, driver})
        // wait for 2 sec
        await driver.manage().setTimeouts({implicit: 2000})
        // Get list of programs (from selected department)
        return await getProgramsList({driver, programsSelector: PROGRAM_SELECTOR})
    }
}

function getTimetableScraper({course}) {
    const {courseCode: program, semester, fullText} = course
    const programsSelector = PROGRAM_SELECTOR

    return async ({driver}) => {
        // go to..
        await timetableSelectionPage({driver})
        // Select "Programmes" menu option
        await clickCss({selector: '#LinkBtn_programmesofstudy', driver})
        // Select "Programme" (course)
        await selectProgram({driver, program, semester, programsSelector})
            .catch(async err => {
                console.log(err)
                await selectProgramFallBack({driver, program: fullText, programsSelector})
            })

        // Select timetable layout
        await changeTimetableLayoutToList({driver, layoutSelector: TT_LAYOUT_SELECTOR})
        // get timetable page
        await driver.findElement(By.css('#bGetTimetable')).click()
        // take timetable data
        const modulesElements = await driver.findElements(By.css('body > table'))
        // wait for 2 sec
        await driver.manage().setTimeouts({implicit: 2000})
        debug(`getTimetableScraper: time table size: ${modulesElements.length}`)

        // take one week time table and convert to array of objects
        const weekModules = await Promise.all(modulesElements)
            .then(async e => {
                return {
                    monday: await e[1].findElements(By.css('table > tbody > tr')),
                    tuesday: await e[2].findElements(By.css('table > tbody > tr')),
                    wednesday: await e[3].findElements(By.css('table > tbody > tr')),
                    thursday: await e[4].findElements(By.css('table > tbody > tr')),
                    friday: await e[5].findElements(By.css('table > tbody > tr')),
                }
            })
            .then(async e => {
                e.monday = await Promise.all(e.monday).then(async arr => await toModuleList(arr))
                e.tuesday = await Promise.all(e.tuesday).then(async arr => await toModuleList(arr))
                e.wednesday = await Promise.all(e.wednesday).then(async arr => await toModuleList(arr))
                e.thursday = await Promise.all(e.thursday).then(async arr => await toModuleList(arr))
                e.friday = await Promise.all(e.friday).then(async arr => await toModuleList(arr))
                return e
            })
            .then(async e => {
                e.monday = await e.monday
                e.tuesday = await e.tuesday
                e.wednesday = await e.wednesday
                e.thursday = await e.thursday
                e.friday = await e.friday
                return e
            })
            .then( e => {
                const arr = []
                arr.push({weekday: "MONDAY", lectures: e.monday})
                arr.push({weekday: "TUESDAY", lectures: e.tuesday})
                arr.push({weekday: "WEDNESDAY", lectures: e.wednesday})
                arr.push({weekday: "THURSDAY", lectures: e.thursday})
                arr.push({weekday: "FRIDAY", lectures: e.friday})
                return arr
            })

        return weekModules
    }

}


const scrape = async ({scraper}) => {
    // https://stackoverflow.com/a/48677891/5322506
    const screen = {
        width: 1000,
        height: 640
    };

    chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build())

    let driver = await new Builder()
        // .forBrowser('chrome')
        .forBrowser('firefox')
        .setChromeOptions(new chrome.Options().headless().windowSize(screen))   // headless
        .setFirefoxOptions(new firefox.Options().headless().windowSize(screen))
        .build();
    try {
        // go to timetable site
        await driver.get(WEB_ADDRESS)
        // use given scraper
        return await scraper({driver})
    } finally {
        await driver.quit()
    }
}


// void(async () =>{
//     const departments = await scrape({scraper: departmentsScraper})
//     debug(`Departments: ${departments.length}`)
//     debug(departments)
//     //
//     // const department = departments[13]  // 10
//     // const courses = await scrape({scraper: getProgramsScraper({department})})
//     // debug(`Programs [${departments.length}] for: \n\t ${department}:`)
//     // debug(courses)
//
//     // const courseIndex = 0
//     // const courseData = courses[courseIndex].split(' ') //19
//
//     const text = 'MA-ART2-Contempory Art Practice (PT) Yr 2'
//     const courseData = text.split(' ') //19
//
//     let semester = courseData.slice(-1).pop()
//     let year = 0
//     const textSem = courseData.pop()    // "Sem"
//
//     if(textSem === "Sem"){
//         year = courseData.pop()
//         courseData.pop()    // "Yr"
//     }
//     else {
//         year = semester
//         semester = 0
//         courseData.pop()    // "Yr"
//     }
//
//     const courseCode = courseData.shift()
//     const name = courseData.join(' ')
//
//     const course = {
//         courseCode,
//         name,
//         year,
//         semester,
//         fullText: text
//     }
//     const tt = await scrape({scraper: getTimetableScraper({course})})
//     debug(`Time table for:`)
//     debug(course)
//     debug(tt)
//     tt.forEach(t=> debug(t.lectures))
//
// })()


exports.Scraper = {
    scrape,
    departmentsScraper,
    getProgramsScraper,
    getTimetableScraper,
    debug,
}



/*
Ref:
    - Logging : https://blog.risingstack.com/node-js-logging-tutorial/
    - Selenium: https://selenium.dev/documentation/en/webdriver/waits/
    - gRPC: https://grpc.io/docs/tutorials/basic/node/
    - gRPC: https://github.com/grpc/grpc-node
 */