/*
     Command to generate files:
```bash

grpc_tools_node_protoc   \
--proto_path=../protos/ \
--proto_path=./ \
--js_out=import_style=commonjs,binary:./target/  \
--grpc_out=./target   \
--plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin`  \
../protos/timetable_scraper.proto

```

 */


// grpc
const grpc = require('grpc')
// debug logger
const debug = require('debug')('gRPC-server-debug')
const warn = require('debug')('gRPC-server-warn')
const info = require('debug')('gRPC-server-info')

// generated code
const messages = require('../target/timetable_scraper_pb')
const services = require('../target/timetable_scraper_grpc_pb')
const gmit = require('../target/gmit_pb')

const Scraper = require('./selenium').Scraper


/**
 * Implements the gRPC method
 * `rpc scrapeDepartments(google.protobuf.Empty) returns (stream ie.gmit.Department) {}`
 *
 */
function scrapeDepartments(call){
    info("Scraper Server: scrapeDepartments")

    Scraper.scrape({scraper: Scraper.departmentsScraper})
        .then(async arr => {
            info(`Scraped ${arr.length} departments.`)
            // closure for getting Department type
            const getType = (typeText) => {
                switch (typeText.toUpperCase()) {
                    // `ART` department has no separate part in text to indicate
                    // what it is ART department. So is why I picked first word in
                    // text to guess it. Ward was "Centre"
                    case 'CENTRE': return gmit.Department.Type.ART
                    case 'BUSINESS': return gmit.Department.Type.BUSINESS
                    case 'ENGINEERING': return gmit.Department.Type.ENGINEERING
                    case 'HOTEL': return gmit.Department.Type.HOTEL
                    case 'SCIENCE': return gmit.Department.Type.SCIENCE
                    default: return gmit.Department.Type.UNKNOWN_DEPARTMENT
                }
            }

            for await(const e of arr){
                const data = e.split(' ')
                const department = new gmit.Department()
                switch(data[0].toUpperCase()){
                    // Galway campus
                    case 'GALWAY': {
                        department.setCampus(gmit.Department.Campus.GALWAY)
                        const type = getType(data[3])
                        department.setType(type)
                        // handle not standard ART department text
                        let artOffset = 0
                        if(type === gmit.Department.Type.ART){
                            artOffset = -2
                        }
                        department.setName(data.slice(5 + artOffset).join(' '))
                        break
                    }
                    // Letterfrack campus has no departments
                    case 'LETTERFRACK': {
                        department.setCampus(gmit.Department.Campus.LETTERFRACK)
                        department.setType(gmit.Department.Type.UNKNOWN_DEPARTMENT)
                        department.setName(data.slice(3).join(' '))
                        break
                    }
                    // Mayo campus has no departments and department text is just: "Mayo campus"
                    case 'MAYO': {
                        department.setCampus(gmit.Department.Campus.MAYO)
                        department.setType(gmit.Department.Type.UNKNOWN_DEPARTMENT)
                        department.setName(data.join(' '))
                        break
                    }
                    default: {
                        department.setCampus(gmit.Department.Campus.UNKNOWN_CAMPUS)
                        department.setType(gmit.Department.Type.UNKNOWN_DEPARTMENT)
                        department.setName(`Unknown Campus: ${e}`)
                    }
                }

                call.write(department)
            }
        })
        .catch(err=> console.log({err}))
        .finally(()=> call.end())
} //returns (stream ie.gmit.Department) {}

/**
 * Implements the gRPC method:
 * `rpc scrapePrograms(ie.gmit.Department) returns (stream ie.gmit.Course) {}`
 * @param call
 */
function scrapePrograms(call){
    info("Scraper Server: scrapePrograms")
    // get department name
    const department = call.request.getName()

    info('department programs: '+ department)
    Scraper.scrape({scraper: Scraper.getProgramsScraper({department})})
        .then(async arr => {
            info(`Scraped ${arr.length} courses`)
            for await (const e of arr){
                const courseData = e.split(' ')

                let semester = courseData.pop()
                let year = 0
                const textSem = courseData.pop()    // "Sem"

                if(textSem === "Sem"){
                    year = courseData.pop()
                    const yr = courseData.pop()    // "Yr"
                    if(yr !== 'Yr'){
                        warn(`Course has no "Yr" in description! Got: ${yr} from description: ${e}`)
                    }
                }
                else {
                    warn(`Course has no "Sam" in description! Got: ${textSem}from description: ${e}`)
                    year = semester
                    semester = 0
                    const yr = courseData.pop()    // "Yr"
                    if(yr !== 'Yr'){
                        warn(`Course has no "Yr" in description! Got: ${yr} from description: ${e}`)
                    }
                }

                const courseCode = courseData.shift()
                const name = courseData.join(' ')

                const course = new gmit.Course()
                course.setCoursecode(courseCode)
                course.setName(name)
                // if not number set to 0
                course.setYear(isNaN(year) ? 0 : year)
                course.setSemester(isNaN(semester) ? 0 : semester)
                course.setNotes(e)
                call.write(course)
            }
        })
        .catch(err=> console.log({err}))
        .finally(()=> call.end())
} //returns (stream ie.gmit.Course) {}


/**
 * Implements the gRPC method
 * `rpc scrapeTimetable(ie.gmit.Course) returns (stream ie.gmit.Lecture) {}`
 * @param call
 */
function scrapeTimetable(call){
    info("Scraper Server: scrapeTimetable")

    const course = {    // {courseCode: program, semester, fullText} = course
        courseCode: call.request.getCoursecode(),
        semester: call.request.getSemester(),
        fullText: call.request.getNotes(),
    }

    Scraper.scrape({scraper: Scraper.getTimetableScraper({course})})
        .then(async arr => {
            info(`Scraped ${arr.length} days`)
            for await (const e of arr){
                // e: `{ weekday: 'FRIDAY', lectures: [] }`
                const weekday = e.weekday
                const lectures = e.lectures
                info(`Here are ${lectures.length} lectures for the ${weekday}`)
                // closure to get `weekday`
                const getWeekday = () => {
                    switch (weekday) {
                        case "MONDAY": return gmit.Weekday.MONDAY
                        case "TUESDAY": return gmit.Weekday.TUESDAY
                        case "WEDNESDAY": return gmit.Weekday.WEDNESDAY
                        case "THURSDAY": return gmit.Weekday.THURSDAY
                        case "FRIDAY": return gmit.Weekday.FRIDAY
                    }
                }

                // for every days lecture loop trough each lecture
                for (const lec of lectures){
                    // build `Module`
                    const module = new gmit.Module()
                    module.setCoursecode(lec.courseCode)
                    module.setModulename(lec.moduleName)
                    module.setType(lec.moduleType)
                    module.setWeeks(lec.weeks)
                    module.setNotes(`${lec.courseCode}, ${lec.moduleName}, ${lec.moduleType}, ${lec.weeks},`)
                    // build `Room`
                    const room = new gmit.Room()
                    const roomNo = Number(lec.room.split(' ')[0])
                    room.setNumber(roomNo)
                    room.setDescription(lec.room)
                    // build `Lecturer`
                    const staff = new gmit.Lecturer()
                    staff.setName(lec.staff.split(' ')[0])
                    staff.setSurname(lec.staff.split(' ').slice(1).join(' '))
                    // staff.setEmail('')
                    // build `Lecture` by putting all build data to it
                    const lecture = new gmit.Lecture()
                    lecture.setRoom(room)
                    let startTime = lec.startTime.split(':')[0]
                    startTime = isNaN(startTime) ? 0 : startTime *1
                    lecture.setStarttime(startTime)

                    let duration = lec.duration.split(':')[0]
                    duration = isNaN(duration) ? 0 : duration *1
                    lecture.setDuration(duration)

                    lecture.setModule(module)
                    lecture.setStaff(staff)
                    lecture.setWeekday(getWeekday())
                    // lecture.setNotes('')

                    // info('\n-----')
                    // info(lec)
                    // info(lecture)
                    call.write(lecture)
                }
            }
        })
        .catch(err=> console.log({err}))
        .finally(()=> call.end())
} //returns (stream ie.gmit.Lecture) {}

function scrapeAllData(call, callback){
    debug("Scraper Server: scrapeAllData")
    const allData = new messages.AllData()
    const department = new gmit.Department()
    department.setName("department name")
    const department2 = new gmit.Department()
    department2.setName("department2 name")
    allData.setDepartmentsList([department, department2])

    callback(null, allData)
} //returns (AllData) {}


/**
 * Starts an RPC server that receives requests for the TimetableScraperService service at the
 * sample server port
 */
function main() {
    const server = new grpc.Server()
    server.addService(services.TimetableScraperService, {
        // getTimetable: getTimetable,
        scrapeDepartments: scrapeDepartments,
        scrapePrograms: scrapePrograms,
        scrapeTimetable: scrapeTimetable,
        scrapeAllData: scrapeAllData,
    })
    server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure())
    info("starting server...")
    server.start()
}

main()


/*
    Request from client object example:
    {
    ...
    request: {
          wrappers_: null,
          messageId_: undefined,
          arrayIndexOffset_: -1,
          array: [],
          pivot_: 1.7976931348623157e+308,
          convertedPrimitiveFields_: {}
        }
    }
 */


