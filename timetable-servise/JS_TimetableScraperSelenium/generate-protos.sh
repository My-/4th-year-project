#!/bin/bash

PROTO_PATH=../protos
GRPC_OUT=./target

mkdir -p $GRPC_OUT

grpc_tools_node_protoc   \
--proto_path=$PROTO_PATH \
--proto_path=./ \
--js_out=import_style=commonjs,binary:$GRPC_OUT  \
--grpc_out=$GRPC_OUT  \
--plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin`  \
$PROTO_PATH/timetable_scraper.proto $PROTO_PATH/gmit.proto