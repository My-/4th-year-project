package ie.gmit.timetable.storge;

import ie.gmit.Course;
import ie.gmit.Department;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Flow;

class PgStorageTest {
    private TimetableStorage storage;

    @BeforeEach
    void setUp() {
        var connector = PgConnector.builder()
//                .host("192.168.1.6")
                .database("timetable_test")
                .password("postgres")
                .user("postgres")
                .build();
        this.storage = PgStorage.of();
        this.storage.connect();
    }

    @SneakyThrows
    @AfterEach
    void tearDown() {
        Thread.sleep(5000);
        this.storage.closeConnection();
    }

    @Test
    void queryRunner(){
        PgStorage.of().runQuery()
        .subscribe(System.out::println);

    }

//    void createDB(){
//        var connector = PgConnector.builder()
////                .database("timetable_gmit")
//                .password("postgers")
//                .user("postgres")
//                .build();
//        this.storage = PgStorage.of(connector);
//        this.storage.connect();
//    }

//    @Test
//    void getDepartment() {
//    }
//
//    @Test
//    void getPrograms() {
//    }
//
//    @Test
//    void getTimetable() {
//    }
//
//    @Test
//    void getDayTimetable() {
//    }
//
//    @Test
//    void getRoomTimetable() {
//    }
//
//    @Test
//    void getModules() {
//    }
//
//    @Test
//    void getCourseStaff() {
//    }
//
//    @Test
//    void getNextLecture() {
//    }

    @Test
    void addDepartment() {
        var department = Department.of(
                ie.gmit.proto.Department.newBuilder()
                        .setName("Test-dep-name-4")
                        .setType(ie.gmit.proto.Department.Type.ART)
                        .setCampus(ie.gmit.proto.Department.Campus.GALWAY)
                        .build()
        );

//        Flow.Subscriber subscriber =

        this.storage.addDepartment(department);
    }

    @Test
    void addProgram() {
        var course = Course.of(
                ie.gmit.proto.Course.newBuilder()
                        .setCourseCode("code-1")
                        .setSemester(1)
                        .setYear(2)
                        .setName("course-1")
                        .build()
        );

        this.storage.addProgram(course);
    }

    @Test
    void updateTimetable() {
    }
}