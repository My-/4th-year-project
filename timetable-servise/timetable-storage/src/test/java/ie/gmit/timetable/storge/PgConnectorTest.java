package ie.gmit.timetable.storge;


//import ie.gmit.proto.Department;
//import ie.gmit.Department;

import ie.gmit.timetable.storge.configuration.StorageConfig;
import ie.gmit.timetable.storge.models.Course;
import ie.gmit.timetable.storge.models.Department;
import io.r2dbc.spi.Connection;
import io.r2dbc.spi.ConnectionFactory;
import io.vertx.junit5.VertxTestContext;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.data.r2dbc.query.Criteria;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.test.StepVerifierExtensionsKt;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class PgConnectorTest {
    private DatabaseClient client;

    @BeforeEach
    void setUp() {
        log.info("Starting tests..");
        var connector = PgConnector.builder()
                .host("192.168.1.6")
                .database("timetable_test")
                .password("postgres")
                .user("postgres")
                .build();
        this.client = connector.create();
    }

    @SneakyThrows
    @AfterEach
    void tearDown() {
        log.info("Test are done!");
        Thread.sleep(3000);
//        client.close();
    }

    @Test
    void insertData() {
        log.info("Test: insertData()");
        var department = ie.gmit.Department.of(
                ie.gmit.proto.Department.newBuilder()
                        .setName("Test-dep-name-4")
                        .setType(ie.gmit.proto.Department.Type.ART)
                        .setCampus(ie.gmit.proto.Department.Campus.GALWAY)
                        .build()
        );
        var query = "";

        client.insert()
                .into(Department.class)
                .table("departments")
                .using(Department.from(department))
                .then()
                .subscribe(System.out::println)
        ;
    }

    @Test
    void insertCourse_r2dbs() throws InterruptedException {
        log.info("Test: insertCourse_r2dbs");

        ConnectionFactory connectionFactory = StorageConfig.builder()
                .build().connectionFactory();

//        Publisher<? extends Connection> connectionPublisher = connectionFactory.create();

        DatabaseClient client = DatabaseClient.create(connectionFactory);

        var department = Department.from(
                ie.gmit.Department.of(
                        ie.gmit.proto.Department.newBuilder()
                                .setName("Test-dep-name-3")
                                .setType(ie.gmit.proto.Department.Type.ART)
                                .setCampus(ie.gmit.proto.Department.Campus.GALWAY)
                                .build()
                )
        );

        var course = Course.from(
                ie.gmit.Course.of(
                        ie.gmit.proto.Course.newBuilder()
                                .setName("course-1")
                                .setYear(1)
                                .setSemester(1)
                                .build()
                )
        );
//        course.setDepartmentId(department.getId());
//
//        Mono<Void> insertionDepartment = client.insert()
//                .into(Department.class)
//                .table("departments")
//                .using(department)
//                .then();
//
//        Mono<Void> insertionDepartment = client.execute(
//                "INSERT INTO departments (campus, type, name, notes)" +
//                        "VALUES(:campus, :type, :name, :notes)"
//        )
//                .bind("campus", department.getCampus())
//                .bind("type", department.getType())
//                .bind("name", department.getName())
//                .bind("notes", department.getNotes())
//                .then();

        var res = client.select()
                .from("departments")
                .matching(
                        Criteria.where("campus").is(department.getCampus())
                                .and("name").is(department.getName())
                                .and("type").is(department.getType())
                )
                .as(Department.class)
                .fetch()
                .all()
                .flatMap(dep ->
                    client.execute(
                            "INSERT INTO courses (id, department_id, name, year, semester, notes)" +
                                    " VALUES(:id, :department_id, :name, :year, :semester, :notes)"
                    )
                            .bind("id", "course-id-here-3")
                            .bind("department_id", dep.getId())
                            .bind("name", course.getName())
                            .bind("year", course.getYear())
                            .bind("semester", course.getSemester())
                            .bind("notes", "notes")
                            .then()
                );

        StepVerifier.create(res)
                .expectComplete()
                .verify(Duration.of(5, ChronoUnit.SECONDS));

//                .subscribe(System.out::println);

//        Mono<Void> insertionCourse = client.insert()
//                .into(Course.class)
//                .table("courses")
//                .using(course)
//                .then();

//        insertionDepartment.subscribe(System.out::println);
//        insertionCourse.subscribe(System.out::println);

//        Thread.sleep(3000);
    }

    @Test
    void connect() throws Throwable {
        log.info("Test: connect()");
//        VertxTestContext testContext = new VertxTestContext();
//        var query = "-- show databases\n" +
//                "SELECT datname\n" +
//                "FROM pg_database\n" +
//                ";";
//        var query ="SELECT * FROM room";
        var query = "SELECT * FROM departments";

//        var query = "";
        Hooks.onOperatorDebug();
//
//        Mono<Integer> all =
        client.execute(query)
//                        .as(Room.class)
                .map((row, rowMetadata) -> {
                    var s = "";
                    s += rowMetadata.getColumnMetadata(1);
                    s += row.get(1);
//                            var id =  (Integer) row.get(0, rowMetadata.getColumnMetadata(0).getJavaType());
//                            String campus =  row.get(1, rowMetadata.getColumnMetadata(1).getJavaType());
//                            String type =  row.get("type", rowMetadata.getColumnMetadata(2).getJavaType());
//                            String name =  (String)row.get(3, rowMetadata.getColumnMetadata(3).getJavaType());
//                            String notes =  (String)row.get(4, rowMetadata.getColumnMetadata(4).getJavaType());
//
//                            var d =  Department.from(
//                                    ie.gmit.Department.of(
//                                            ie.gmit.proto.Department.newBuilder()
//                                                    .setCampus(ie.gmit.proto.Department.Campus.valueOf(campus))
//                                                    .setType(ie.gmit.proto.Department.Type.valueOf(type))
//                                                    .setName(Optional.ofNullable(name).orElse(""))
//                                                    .setNotes(Optional.ofNullable(notes).orElse(""))
//                                                    .build()
//                                    )
//                            );
//                            d.setDepartmentId(id);

//                            return d;
                    return s;
                })
                .all()
                .subscribe(System.out::println);
////
//                // PostgresqlRow{codecs=io.r2dbc.postgresql.codec.DefaultCodecs@5fc1b99f,
//                // columns=[
//                // Field{column=1, dataType=23, dataTypeModifier=-1, dataTypeSize=4, format=FORMAT_TEXT, name='department_id', table=25243},
//                // Field{column=2, dataType=25183, dataTypeModifier=-1, dataTypeSize=4, format=FORMAT_TEXT, name='campus', table=25243},
//                // Field{column=3, dataType=25192, dataTypeModifier=-1, dataTypeSize=4, format=FORMAT_TEXT, name='type', table=25243},
//                // Field{column=4, dataType=1043, dataTypeModifier=259, dataTypeSize=-1, format=FORMAT_TEXT, name='name', table=25243},
//                // Field{column=5, dataType=1043, dataTypeModifier=259, dataTypeSize=-1, format=FORMAT_TEXT, name='raw_text', table=25243},
//                // Field{column=6, dataType=1114, dataTypeModifier=-1, dataTypeSize=8, format=FORMAT_TEXT, name='created_on', table=25243},
//                // Field{column=7, dataType=1114, dataTypeModifier=-1, dataTypeSize=8, format=FORMAT_TEXT, name='updated_on', table=25243}],
//                // isReleased=false}
////
////                .as(Department.class)
////                .fetch()

//        ;


//        PgConnectOptions connectOptions = new PgConnectOptions()
//                .setPort(5432)
////                .setHost("192.168.1.6")
//                .setHost("localhost")
//                .setDatabase("timetable_test")
//                .setUser("postgres")
//                .setPassword("postgres");
//
//// Pool options
//        PoolOptions poolOptions = new PoolOptions()
//                .setMaxSize(5);
//
//// Create the pooled client
//        PgPool client = PgPool.pool(connectOptions, poolOptions);
//
//        Flux<Department> flux = Flux.empty();
//
//        client.query(query, ar -> {
//            if (ar.succeeded()) {
//
//                RowSet<Row> result = ar.result();
//                System.out.println("Got " + result.size() + " rows ");
////                System.out.println(result);
//                log.info("Got: {}", result.size());
//
//
////                for( var s : result){
////
////                }
//
////                var re =
//                StreamSupport.stream(result.spliterator(), false)
//                        .map(it -> {
//                            var d =  Department.from(
//                                    ie.gmit.Department.of(
//                                            ie.gmit.proto.Department.newBuilder()
//                                                    .setCampus(ie.gmit.proto.Department.Campus.valueOf(it.getString(1)))
//                                                    .setType(ie.gmit.proto.Department.Type.valueOf(it.getString(2)))
//                                                    .setName(it.getString(3))
//                                                    .setNotes(it.getString(4) == null ? "" : it.getString(4))
//                                                    .build()
//                                    )
//                            );
//                            d.setDepartmentId(it.getInteger(0));
//                            return d;
//                        })
//                        .forEach(System.out::println);
//
//            } else {
//                System.out.println("Failure: " + ar.cause().getMessage());
//                log.warn("Failure: " + ar.cause().getMessage());
//            }
//        });


//                        .peek(System.out::println)
//                        .anyMatch(it -> it.getValue(0).toString().equals("timetable_test5"));

//                Consumer<Department> cons = it -> {
//                    log.info("- {}", it);
//                };
//
////                flux.zipWith(Flux.fromIterable(result))
////                        .subscribe(System.out::println);;
//
////                System.out.println(re);
//
////                testContext.verify(() -> assertTrue(re));
////                testContext.completeNow();
//


//        if (testContext.failed()) {
//            throw testContext.causeOfFailure();
//        }
//        Consumer<Department> cons = it -> {
//            log.info("- {}", it);
//        };

//        flux
//                .parallel()
//                .subscribe(cons);
    }
}