package ie.gmit.timetable.storge;


import io.vertx.core.Vertx;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.data.r2dbc.core.DatabaseClient;

import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Ref:
 * - https://vertx.io/docs/guide-for-java-devs/#_testing_vert_x_code
 * - https://developers.redhat.com/blog/2018/01/23/vertx-junit5-async-testing/
 */

@Slf4j
@DisplayName("👋 Testing Vert.x driver")
@ExtendWith(VertxExtension.class)
class PgConnectorTest_Async {
    private DatabaseClient client;
//    Checkpoint serverStarted;
//    Checkpoint requestsServed;
//    Checkpoint responsesReceived;


    @BeforeEach
    void setUp(Vertx vertx, VertxTestContext testContext) {
        log.info("Started testing..");
        var connector = PgConnector.builder()
                .database("timetable_test")
                .password("postgers")
                .user("postgres")
                .build();
        this.client = connector.create();

//        serverStarted = testContext.checkpoint();
//        requestsServed = testContext.checkpoint(10);
//        responsesReceived = testContext.checkpoint(10);
    }

    @SneakyThrows
    @AfterEach
    void tearDown(Vertx vertx, VertxTestContext testContext) {
        log.info("End testing!");
//        Thread.sleep(2000);
        testContext.awaitCompletion(5, TimeUnit.SECONDS);

        if (testContext.failed()) {
            throw testContext.causeOfFailure();
        }
//        client.close();
    }

    @Test
//    @RepeatedTest(3)
//    @Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
    void connect(Vertx vertx, VertxTestContext testContext) throws Throwable {
        log.info("Test:");
//        VertxTestContext testContext = new VertxTestContext();
//        Checkpoint serverStarted = testContext.checkpoint();
//        Checkpoint requestsServed = testContext.checkpoint(10);
//        Checkpoint responsesReceived = testContext.checkpoint(10);

        var query = "-- show databases\n" +
                "SELECT datname\n" +
                "FROM pg_database\n" +
                ";";
//        serverStarted.flag();
//        client.query(query, ar -> {
////            requestsServed.flag();
//            if (ar.succeeded()) {
//
//                RowSet<Row> result = ar.result();
//                System.out.println("Got " + result.size() + " rows ");
//                System.out.println(result);
////                result.forEach(r -> System.out.println(r.getValue(0)));
////                log.info("{}", result);
////                testContext.succeeding()
//
////                var has_it = StreamSupport.stream(result.spliterator(), true)
////                        .anyMatch(r -> r.getValue(1).equals("timetable_test"));
//
////                if (has_it) {
////                testContext.succeeding(res ->
//                testContext.verify(() ->
//                    assertTrue(StreamSupport.stream(result.spliterator(), true)
//                            .anyMatch(r -> r.getValue(1).equals("timetable_test")))
////                                testContext.completeNow();
//                );
////                );
////                responsesReceived.flag();
//                testContext.completeNow();
//
////                }
//
//            } else {
//                System.out.println("Failure: " + ar.cause().getMessage());
//                log.warn("Failure: " + ar.cause().getMessage());
//                testContext.failNow(ar.cause());
//            }
//
//        });
////        testContext.completeNow();
//        testContext.awaitCompletion(5, TimeUnit.SECONDS);
//
//        if (testContext.failed()) {
//            throw testContext.causeOfFailure();
//        }
    }
}