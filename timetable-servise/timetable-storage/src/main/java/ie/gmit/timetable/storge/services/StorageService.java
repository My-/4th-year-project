package ie.gmit.timetable.storge.services;

import ie.gmit.timetable.storge.repository.CourseRepository;
import ie.gmit.timetable.storge.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StorageService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    CourseRepository courseRepository;

    public void doWork(){
        departmentRepository.findAll()
                .subscribe(System.out::println);
    }
}
