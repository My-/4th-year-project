package ie.gmit.timetable.storge.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;


import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Department {
    @Id
    private int id;
    private String name;
    private String notes;
    private String campus;
    private String type;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;


    public static Department from(ie.gmit.Department department){
        return Department.builder()
                .name(department.getName())
                .notes(department.getNotes())
                .campus(department.getCampus().name())
                .type(department.getType().name())
                .build()
                ;
    }






}
