package ie.gmit.timetable.storge.repository;

import ie.gmit.timetable.storge.models.Course;
import ie.gmit.timetable.storge.models.Department;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface CourseRepository extends ReactiveCrudRepository<Course, Long> {
//    @Query("SELECT * FROM course WHERE last_name = :lastname")
    Flux<Course> findByDepartment(Department department);
}
