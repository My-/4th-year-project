package ie.gmit.timetable.storge.configuration;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import lombok.Builder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Builder
@Configuration
@EnableR2dbcRepositories
public class    StorageConfig extends AbstractR2dbcConfiguration {
    @Builder.Default private int port = 5432;
    @Builder.Default private String host = "127.0.0.1";
    @Builder.Default private String database = "timetable_test";
    @Builder.Default private String user = "postgres";
    @Builder.Default private String password = "postgres";

    @Bean
    @Override
    public ConnectionFactory connectionFactory() {
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                        .host(this.host)
//                        .host("192.168.1.6")
                        .database(this.database)
                        .username(this.user)
                        .password(this.password)
                        .build()
        );
//        return ConnectionFactories.get(
//                String.format("r2dbc:postgres://%s:5432/%s", host, database)
//        );

//    ConnectionFactory connectionFactory = ConnectionFactories.get("r2dbc:postgres://<host>:5432/<database>");
//    Publisher<? extends Connection> connectionPublisher = connectionFactory.create();
    }



}
