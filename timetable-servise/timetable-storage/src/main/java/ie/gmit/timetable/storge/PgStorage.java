package ie.gmit.timetable.storge;

import ie.gmit.*;
import ie.gmit.Module;
import ie.gmit.proto.WeekDay;
import ie.gmit.timetable.storge.configuration.StorageConfig;
import ie.gmit.timetable.storge.repository.CourseRepository;
import io.r2dbc.spi.ConnectionFactory;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.data.repository.core.support.ReactiveRepositoryFactorySupport;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;

@Slf4j
public class PgStorage implements TimetableStorage {
    private final ConnectionFactory connectionFactory;
    private DatabaseClient client;

    @Autowired
    CourseRepository courseRepository;

    private PgStorage(){
//        this.connector = connector;
       this.connectionFactory = StorageConfig.builder()
                .build().connectionFactory();
       this.client= DatabaseClient.create(connectionFactory);
    }

    public static PgStorage of(){
        return new PgStorage();
    }

    public Flux<ie.gmit.timetable.storge.models.Course> runQuery(){

        var allCourses = courseRepository.findAll();

        return allCourses;
    }

    @Override
    public void connect() {
//        this.clientPool = connector.connect();
        log.info("{} connected!", this.getClass().getName());
    }

    @Override
    public void closeConnection() {
//        this.clientPool.close();
    }

    @Override
    public Flux<Department> getDepartments() {
        return client.select()
                .from("departments")
                .as(ie.gmit.timetable.storge.models.Department.class)
                .fetch()
                .all()
                ;
    }

    @Override
    public List<Course> getPrograms(Department department) {
        return null;
    }

    @Override
    public List<Lecture> getTimetable(Course course) {
        return null;
    }

    @Override
    public List<Lecture> getDayTimetable(Course course, WeekDay weekDay) {
        return null;
    }

    @Override
    public List<Lecture> getRoomTimetable(Room room) {
        return null;
    }

    @Override
    public List<Module> getModules(Course course) {
        return null;
    }

    @Override
    public List<Lecturer> getCourseStaff(Course course) {
        return null;
    }

    @Override
    public Lecture getNextLecture(Course course) {
        return null;
    }


    @Override
    public void addDepartment(Department department) {
//        var query = "INSERT INTO " +
//                "departments (campus, type, name)" +
//                " VALUES ($1, $2, $3)";
//
//        var values = Tuple.of(
//                department.getCampus().name(),
//                department.getType().name(),
//                department.getName()
//        );
//
//        runQuery(query, values);
    }

    @Override
    public void addProgram(Course course) {
//        var query = "INSERT INTO " +
//                "courses (course_id, name, year, semester)" +
//                " VALUES ($1, $2, $3, $4)";
//
//        var values = Tuple.of(
//                course.getCourseCode(),
//                course.getName(),
//                course.getYear(),
//                course.getSemester()
//        );
//
//        runQuery(query, values);
    }

    @Override
    public void addTimetable(Lecture lecture) {

    }
}
