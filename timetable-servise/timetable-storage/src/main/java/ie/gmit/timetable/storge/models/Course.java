package ie.gmit.timetable.storge.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Course {
    @Id
    private long id;
    private long departmentId;
    private String name;
    private int year;
    private int semester;
    private String notes;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    public static Course from(ie.gmit.Course course){
        return Course.builder()
//                .departmentId(Department.from(course.getDepartment()).getId())
                .name(course.getName())
                .year(course.getYear())
                .semester(course.getSemester())
                .build()
                ;
    }
}
