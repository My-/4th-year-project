package ie.gmit.timetable.storge.repository;

import ie.gmit.timetable.storge.models.Department;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface DepartmentRepository  extends ReactiveCrudRepository<Department, Long> {

    @Query("SELECT * FROM departments WHERE campus = :campus")
    Flux<Department> findByCampus(String campus);


}
