package ie.gmit.timetable.storge;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.Connection;
import io.r2dbc.spi.ConnectionFactory;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import lombok.Builder;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Mono;

import java.util.Map;

@Builder // https://projectlombok.org/features/Builder
public class PgConnector {
    @Builder.Default
    private int port = 5432;
    @Builder.Default
    private String host = "127.0.0.1";
    private String database;
    private String user;
    private String password;


    public DatabaseClient create() {
        // https://spring.io/projects/spring-data-r2dbc#overview
        ConnectionFactory connectionFactory = new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                        .host(this.host)
                        .database(this.database)
                        .username(this.user)
                        .password(this.password)
//                        .options(Map.of("string-type", "unspecified"))
                        .build()
        );

//        PgConnectOptions connectOptions = new PgConnectOptions()
//                .setPort(this.port)
//                .setHost(this.host)
//                .setDatabase(this.database)
//                .setUser(this.user)
//                .setPassword(this.password);
        Publisher<? extends Connection> connectionPublisher = connectionFactory.create();

        Mono<Connection> connectionMono = Mono.from(connectionFactory.create());

        return DatabaseClient.create(connectionFactory);

//        // Pool options
//        PoolOptions poolOptions = new PoolOptions()
//                .setMaxSize(5);
//
//        // Create the client pool
//        return PgPool.pool(connectOptions, poolOptions);
    }
}
