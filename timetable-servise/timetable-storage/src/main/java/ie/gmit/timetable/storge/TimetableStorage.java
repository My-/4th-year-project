package ie.gmit.timetable.storge;

import ie.gmit.tt_service.TimetableService;
import ie.gmit.tt_storage.updater.TimetableStorageAdderService;

public interface TimetableStorage
    extends TimetableService, TimetableStorageAdderService
{
    public void connect();
    public void closeConnection();

}
