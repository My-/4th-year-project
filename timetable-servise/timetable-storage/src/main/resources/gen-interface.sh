#!/bin/bash

# This sript is uses custom codegen to generate
# a Java interfaces form a protofile

SRC_DIR=../java
PROTO_DIR=../proto
PROTO_FILE=tt_storage_adder.proto

./proto-service-4j \
-i  $PROTO_DIR/$PROTO_FILE \
-o $SRC_DIR

# timetable service

PROTO_DIR=../../../../../protos
PROTO_FILE=tt_service.proto

./proto-service-4j \
-i  $PROTO_DIR/$PROTO_FILE \
-o $SRC_DIR