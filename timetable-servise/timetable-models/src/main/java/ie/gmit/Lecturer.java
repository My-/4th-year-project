// This is generated class
package ie.gmit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Lecturer {
	private String name;
	private String surname;
	private String email;
	private String notes;

	private Lecturer(ie.gmit.proto.Lecturer lecturerProto) {
		this.name = lecturerProto.getName();
		this.surname = lecturerProto.getSurname();
		this.email = lecturerProto.getEmail();
		this.notes = lecturerProto.getNotes();
	}

	public static Lecturer of(ie.gmit.proto.Lecturer lecturerProto) {
		return new Lecturer(lecturerProto);
	}

	public ie.gmit.proto.Lecturer toProto() {
		return ie.gmit.proto.Lecturer.newBuilder()
			.setName(this.name)
			.setSurname(this.surname)
			.setEmail(this.email)
			.setNotes(this.notes)
			.build();
	}
}
