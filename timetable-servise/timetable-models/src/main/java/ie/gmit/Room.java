// This is generated class
package ie.gmit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Room {
	private int number;
	private String description;

	private Room(ie.gmit.proto.Room roomProto) {
		this.number = roomProto.getNumber();
		this.description = roomProto.getDescription();
	}

	public static Room of(ie.gmit.proto.Room roomProto) {
		return new Room(roomProto);
	}

	public ie.gmit.proto.Room toProto() {
		return ie.gmit.proto.Room.newBuilder()
			.setNumber(this.number)
			.setDescription(this.description)
			.build();
	}
}
