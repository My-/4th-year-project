// This is generated class
package ie.gmit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Module {
	private String courseCode;
	private String moduleName;
	private ie.gmit.proto.Module.Type type;
	private String weeks;
	private String notes;

	private Module(ie.gmit.proto.Module moduleProto) {
		this.courseCode = moduleProto.getCourseCode();
		this.moduleName = moduleProto.getModuleName();
		this.type = moduleProto.getType();
		this.weeks = moduleProto.getWeeks();
		this.notes = moduleProto.getNotes();
	}

	public static Module of(ie.gmit.proto.Module moduleProto) {
		return new Module(moduleProto);
	}

	public ie.gmit.proto.Module toProto() {
		return ie.gmit.proto.Module.newBuilder()
			.setCourseCode(this.courseCode)
			.setModuleName(this.moduleName)
			.setType(this.type)
			.setWeeks(this.weeks)
			.setNotes(this.notes)
			.build();
	}
}
