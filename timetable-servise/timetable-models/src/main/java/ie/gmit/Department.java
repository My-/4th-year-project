// This is generated class
package ie.gmit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {
	private ie.gmit.proto.Department.Campus campus;
	private ie.gmit.proto.Department.Type type;
	private String name;
	private String notes;

	private Department(ie.gmit.proto.Department departmentProto) {
		this.campus = departmentProto.getCampus();
		this.type = departmentProto.getType();
		this.name = departmentProto.getName();
		this.notes = departmentProto.getNotes();
	}

	public static Department of(ie.gmit.proto.Department departmentProto) {
		return new Department(departmentProto);
	}

	public ie.gmit.proto.Department toProto() {
		return ie.gmit.proto.Department.newBuilder()
			.setCampus(this.campus)
			.setType(this.type)
			.setName(this.name)
			.setNotes(this.notes)
			.build();
	}
}
