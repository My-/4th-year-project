// This is generated class
package ie.gmit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Lecture {
	private Room room;
	private int startTime;
	private int duration;
	private Module module;
	private Lecturer staff;
	private ie.gmit.proto.WeekDay weekDay;
	private String notes;

	private Lecture(ie.gmit.proto.Lecture lectureProto) {
		this.room = Room.of(lectureProto.getRoom());
		this.startTime = lectureProto.getStartTime();
		this.duration = lectureProto.getDuration();
		this.module = Module.of(lectureProto.getModule());
		this.staff = Lecturer.of(lectureProto.getStaff());
		this.weekDay = lectureProto.getWeekDay();
		this.notes = lectureProto.getNotes();
	}

	public static Lecture of(ie.gmit.proto.Lecture lectureProto) {
		return new Lecture(lectureProto);
	}

	public ie.gmit.proto.Lecture toProto() {
		return ie.gmit.proto.Lecture.newBuilder()
			.setRoom(this.room.toProto())
			.setStartTime(this.startTime)
			.setDuration(this.duration)
			.setModule(this.module.toProto())
			.setStaff(this.staff.toProto())
			.setWeekDay(this.weekDay)
			.setNotes(this.notes)
			.build();
	}
}
