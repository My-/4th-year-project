// This is generated class
package ie.gmit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Course {
	private String courseCode;
	private String name;
	private int year;
	private int semester;
	private Department department;
	private String notes;

	private Course(ie.gmit.proto.Course courseProto) {
		this.courseCode = courseProto.getCourseCode();
		this.name = courseProto.getName();
		this.year = courseProto.getYear();
		this.semester = courseProto.getSemester();
		this.department = Department.of(courseProto.getDepartment());
		this.notes = courseProto.getNotes();
	}

	public static Course of(ie.gmit.proto.Course courseProto) {
		return new Course(courseProto);
	}

	public ie.gmit.proto.Course toProto() {
		return ie.gmit.proto.Course.newBuilder()
			.setCourseCode(this.courseCode)
			.setName(this.name)
			.setYear(this.year)
			.setSemester(this.semester)
			.setDepartment(this.department.toProto())
			.setNotes(this.notes)
			.build();
	}
}
