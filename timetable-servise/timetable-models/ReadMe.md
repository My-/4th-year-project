# timetable-models
This project is for holding models (Data Transfer Objects - DTO's).

All code is generated from `.proto` using Maven and [custom codegen][1] to generate wrappers. Wrappers are needed as protobuf generated classes are not working in SpringBoot.  
Classes are generated using `resources/generate.sh` script.
From `timetable-medels` project artefact is created (`.jar` file). In order to use these models other projects should import that `.jar` as library.



[1]: https://gitlab.com/My-/4th-year-project/tree/30-creating-code-gen-in-rust
