package ie.gmit.timetable.controllers;

import ie.gmit.timetable.conf.PostgresConfig;
import ie.gmit.timetable.models.Department;
import ie.gmit.timetable.repositories.DepartmentRepository;
//import ie.gmit.timetable.services.DepartmentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

// ref:
//  - https://howtodoinjava.com/spring-webflux/webfluxtest-with-webtestclient/

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = DepartmentController.class)
@Import({ PostgresConfig.class})
class DepartmentControllerTest {

    @MockBean
    DepartmentRepository repository;

    @Autowired
    private WebTestClient webClient;

    @Test
    void create() {
        var department = Department.builder()
                .campus("test-campus")
                .name("test-department")
                .type("test-type")
                .build()
                ;

        var department1 = Department.builder()
                .campus("test-campus2")
                .name("test-department")
                .type("test-type")
                .build()
                ;

        Mockito.when(repository.save(department)).thenReturn(Mono.just(department));

        webClient.post()
                .uri("/departments")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(department))
                .exchange()
                .expectStatus().isCreated();

        Mockito.verify(repository, times(1)).save(department);
//        Mockito.verify(repository, times(1)).save(department1);
    }

    @Test
    void getAll() {
        var dataSize = 5;
        IntFunction<Department> newDepartment = i ->
                Department.builder()
                    .campus("test-campus-"+ i)
                    .name("test-department-"+ i)
                    .type("test-type- "+ i)
                    .build()
                ;

        var list = IntStream.range(0, 5)
                .mapToObj(newDepartment)
                .collect(Collectors.toList())
                ;

        Flux<Department> employeeFlux = Flux.fromIterable(list);

        Mockito.when(repository.findAll())
                .thenReturn(employeeFlux)
        ;

        webClient.get().uri("/name/{name}", "Test")
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Department.class)
        ;

        Mockito.verify(repository, times(1)).findAll();
    }


}