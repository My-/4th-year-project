package ie.gmit.timetable.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("departments")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {
    @Id
    private int id;
    private String name;
    private String notes;
    private String campus;
    private String type;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    public static Department from(ie.gmit.Department department){
        return Department.builder()
                .name(department.getName())
                .notes(department.getNotes())
                .campus(department.getCampus().name())
                .type(department.getType().name())
                .build()
                ;
    }

    public static Department from(ie.gmit.proto.Department department){
        return Department.builder()
                .name(department.getName())
                .notes(department.getNotes())
                .campus(department.getCampus().name())
                .type(department.getType().name())
                .build()
                ;
    }

    public ie.gmit.proto.Department toProto(){
        return ie.gmit.proto.Department.newBuilder()
                .setName(name)
                .setNotes(notes)
                .setCampus(ie.gmit.proto.Department.Campus.valueOf(campus))
                .setType(ie.gmit.proto.Department.Type.valueOf(type))
                .build()
                ;
    }

    public boolean containsId(){
        return id != 0;
    }
}
