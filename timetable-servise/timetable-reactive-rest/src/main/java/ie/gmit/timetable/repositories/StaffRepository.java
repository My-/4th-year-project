package ie.gmit.timetable.repositories;

import ie.gmit.timetable.models.Course;
import ie.gmit.timetable.models.Staff;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface StaffRepository extends ReactiveCrudRepository<Staff, Integer> {
    @Query("SELECT * FROM staff WHERE name IN(:name) AND surname IN(:surname)")
    Mono<Staff> findOneByNameAndSurname(String name, String surname);
}
