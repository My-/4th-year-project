package ie.gmit.timetable.repositories;

import ie.gmit.timetable.models.Course;
import ie.gmit.timetable.models.Module;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface ModuleRepository  extends ReactiveCrudRepository<Module, Integer> {
    // unique (course_code, name, type)
    @Query("SELECT * FROM modules WHERE course_code IN(:courseCode) AND name IN(:name) AND type IN(:type)")
    Mono<Module> findOneByCourseCodeAndNameAndType(String courseCode, String name, String type);

    @Query("SELECT * FROM modules WHERE notes IN(:notes)")
    Mono<Module> findOneByNotes(String notes);

}
