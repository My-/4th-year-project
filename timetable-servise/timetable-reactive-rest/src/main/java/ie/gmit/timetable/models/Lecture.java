package ie.gmit.timetable.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("lectures")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@IdClass
public class Lecture {
    // primary key (room_no, weekday, start),
    private int moduleId;
    private int roomNo;
    private int staffId;
    private String weekday;
    private int start;
    private int duration;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

//    public static Lecture from(ie.gmit.proto.Lecture lecture) {
//        return Lecture.builder()
//                // no module id
//                .roomNo(lecture.getRoom().getNumber())
//                // no staff id
//                .weekday(lecture.getWeekDay().name())
//                .start(lecture.getStartTime())
//                .duration(lecture.getDuration())
//                .build()
//                ;
//    }
}
