package ie.gmit.timetable.repositories;

import ie.gmit.timetable.models.Department;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DepartmentRepository extends ReactiveCrudRepository<Department, Long> {
    @Query("SELECT * FROM departments WHERE name IN(:name)")
    Flux<Department> findByName(String name);

    @Query("SELECT * FROM departments WHERE type IN(:type)")
    Flux<Department> findByType(String type);

    @Query("SELECT * FROM departments WHERE campus IN(:campus)")
    Flux<Department> findByCampus(String campus);

    /** Should be single department as: {@code unique(name, type, campus)} */
    @Query("SELECT * FROM departments WHERE name = :name AND type = :type AND campus = :campus")
    Mono<Department> findOneByCampusAndNameAndType(String name, String type, String campus);
}
