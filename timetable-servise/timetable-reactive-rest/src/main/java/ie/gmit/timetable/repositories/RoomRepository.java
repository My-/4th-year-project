package ie.gmit.timetable.repositories;

import ie.gmit.timetable.models.Course;
import ie.gmit.timetable.models.Room;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface RoomRepository  extends ReactiveCrudRepository<Room, Integer> {

    @Query("INSERT INTO rooms(room_no, description) VALUES (:roomNo, :description)")
    Flux<Room> insert(int roomNo, String description);
}
