package ie.gmit.timetable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import reactor.core.publisher.Hooks;

@SpringBootApplication //(scanBasePackages={"ie.gmit.timetable"})
//@ComponentScan(basePackages = "ie.gmit.timetable")
//@EnableR2dbcRepositories("ie.gmit.timetable")
@EnableR2dbcRepositories(basePackages = "ie.gmit.timetable")
public class TimetableApplication {

    public static void main(String[] args) {
//        Hooks.onOperatorDebug();
        SpringApplication.run(TimetableApplication.class, args);
    }



}
