package ie.gmit.timetable.conf;

import ie.gmit.tt_scraper.TT_ScraperClient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
@ConfigurationProperties("timetable.scraper.grps-server")
public class TimetableScraperConfig {
    private String host;
    private int port;

    @Bean
    TT_ScraperClient timetableScraper(){
        log.info("Creating Timetable Scraper bean. Address: {}:{}", host, port);
        return TT_ScraperClient.of(host, port);
    }
}
