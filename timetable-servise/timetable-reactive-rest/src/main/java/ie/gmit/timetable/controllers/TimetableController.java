package ie.gmit.timetable.controllers;


import ie.gmit.timetable.models.*;
import ie.gmit.timetable.models.Module;
import ie.gmit.timetable.repositories.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/timetable")
public class TimetableController {

    @Qualifier("courseRepository")
    private final CourseRepository courseRepository;

    @Qualifier("departmentRepository")
    private final DepartmentRepository departmentRepository;

    @Qualifier("lectureRepository")
    private final LectureRepository lectureRepository;

    @Qualifier("departmentRepository")
    private final ModuleRepository moduleRepository;

    @Qualifier("departmentRepository")
    private final RoomRepository roomRepository;

    @Qualifier("departmentRepository")
    private final StaffRepository staffRepository;

    public TimetableController(CourseRepository courseRepository,
                               DepartmentRepository departmentRepository,
                               LectureRepository lectureRepository,
                               ModuleRepository moduleRepository,
                               RoomRepository roomRepository,
                               StaffRepository staffRepository
    ) {
        this.courseRepository = courseRepository;
        this.departmentRepository = departmentRepository;
        this.lectureRepository = lectureRepository;
        this.moduleRepository = moduleRepository;
        this.roomRepository = roomRepository;
        this.staffRepository = staffRepository;
    }

    /**
     * /// Get available departments
     * rpc getDepartments(google.protobuf.Empty) returns (stream ie.gmit.Department) {}
     *
     * @return
     */
    @GetMapping("/departments")
    Flux<Department> getAllDepartments() {
        return this.departmentRepository.findAll();
    }

    /**
     * /// Get available courses for a given department
     * rpc getPrograms(ie.gmit.Department) returns (stream ie.gmit.Course) {
     *
     * @param department
     * @return
     */
    @GetMapping("/courses")
    Flux<Course> getAllPrograms(@RequestBody Department department) {
        log.info("getAllPrograms: {}", department);
        return departmentRepository
                .findOneByCampusAndNameAndType(
                        department.getName(),
                        department.getType(),
                        department.getCampus()
                )
                .map(Department::getId)
                .flatMapMany(courseRepository::findAllByDepartmentId)
                ;
    }

    /**
     * /// Get weeks timetable for a given course
     * rpc getTimetable(ie.gmit.Course) returns (stream ie.gmit.Lecture) {}
     *
     * @param course
     * @return
     */
    @GetMapping("/lectures")
    Flux<Lecture> getTimetable(@RequestBody Course course) {
        log.info("getTimetable: {}", course);
        return lectureRepository
                .findAll()
                ;
    }

    /**
     * /// Get timetable for a specific day for a specific course
     * rpc getDayTimetable(DayTimetableRequest) returns (stream ie.gmit.Lecture) {}
     *
     * @param course
     * @return
     */
    @GetMapping("/lectures/{weekday}")
    Flux<Lecture> getDayTimetable(@PathVariable String weekday,
                                  @RequestBody Course course
    ) {
        return this.lectureRepository.findAll();
    }

    /**
     * /// Get timetable for a given room
     * rpc getRoomTimetable(ie.gmit.Room) returns (stream ie.gmit.Lecture) {}
     *
     * @param room
     * @return
     */
    @GetMapping("/lectures/")
    Flux<Lecture> getRoomTimetable(@RequestParam Room room) {
        return this.lectureRepository.findAll();
    }

    /**
     * /// Get all modules for a given course
     * rpc getModules(ie.gmit.Course) returns (stream ie.gmit.Module) {}
     *
     * @param course
     * @return
     */
    @GetMapping("/modules")
    Flux<Module> getModules(@RequestBody Course course) {
        return this.moduleRepository.findAll();
    }

    /**
     * /// Get all staff for a given course
     * rpc getCourseStaff(ie.gmit.Course) returns (stream ie.gmit.Lecturer) {}
     *
     * @param course
     * @return
     */
    @GetMapping("/staff")
    Flux<Staff> getCourseStaff(@RequestBody Course course) {
        return this.staffRepository.findAll();
    }

    /**
     * /// Get next lecture for a given course
     * rpc getNextLecture(ie.gmit.Course) returns (ie.gmit.Lecture) {}
     *
     * @param course
     * @return
     */
    @GetMapping("/lectures/next")
    Flux<Lecture> getNextLecture(@RequestBody Course course) {
        return this.lectureRepository.findAll();
    }
}
