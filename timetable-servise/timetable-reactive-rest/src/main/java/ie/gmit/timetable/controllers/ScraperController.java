package ie.gmit.timetable.controllers;

import ie.gmit.timetable.models.*;
import ie.gmit.timetable.models.Module;
import ie.gmit.timetable.repositories.*;
import ie.gmit.tt_scraper.TT_ScraperClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/scraper")
public class ScraperController {
    @Qualifier("courseRepository")
    private final CourseRepository courseRepository;

    @Qualifier("departmentRepository")
    private final DepartmentRepository departmentRepository;

    @Qualifier("lectureRepository")
    private final LectureRepository lectureRepository;

    @Qualifier("departmentRepository")
    private final ModuleRepository moduleRepository;

    @Qualifier("departmentRepository")
    private final RoomRepository roomRepository;

    @Qualifier("departmentRepository")
    private final StaffRepository staffRepository;

    private final TT_ScraperClient scraperClient;

    public ScraperController(CourseRepository courseRepository,
                             DepartmentRepository departmentRepository,
                             LectureRepository lectureRepository,
                             ModuleRepository moduleRepository,
                             RoomRepository roomRepository,
                             StaffRepository staffRepository,
                             TT_ScraperClient scraperClient) {
        this.courseRepository = courseRepository;
        this.departmentRepository = departmentRepository;
        this.lectureRepository = lectureRepository;
        this.moduleRepository = moduleRepository;
        this.roomRepository = roomRepository;
        this.staffRepository = staffRepository;
        this.scraperClient = scraperClient;
    }

    @PostMapping("/departments")
    Flux<Department> scrapeDepartments() {
        log.info("scrapeDepartments");
        return scraperClient.scrapeDepartments()
                .map(Department::from)
                .flatMap(department -> departmentRepository.save(department)
                        .onErrorResume(throwable -> {
                            log.warn("Problem inserting department: {}, whith error: {}", department, throwable.getMessage());
                            return departmentRepository.findOneByCampusAndNameAndType(
                                    department.getName(), department.getType(), department.getCampus()
                            );
                        })
                )
                ;
    }

    @PostMapping("/courses/{departmentId}")
    Flux<Course> scrapeCourses(@PathVariable long departmentId) {
        log.info("scrapeCourses() for: {}", departmentId);
        return departmentRepository.findById(departmentId)
                .flatMapMany(department -> scraperClient.scrapePrograms(department.toProto())
                        .map(Course::from)
                        .map(course -> {
                            course.setDepartmentId(department.getId());
                            return course;
                        })
                )
                .filter(Course::isSecondTerm)
                .flatMap(course -> courseRepository.insert(
                                course.getCourseCode(),
                                course.getDepartmentId(),
                                course.getName(),
                                course.getYear(),
                                course.getSemester(),
                                course.getNotes()
                        )
                        .onErrorResume(throwable -> {
                            log.warn("Course insert error: {}, err: {}", course, throwable.getMessage());
                            return courseRepository.findById(course.getCourseCode());
                        })
                        .doOnError(err -> log.warn("Course insert error: {}, err: {}", course, err.getMessage()))
                )
//                .onErrorContinue((err, cause) -> log.error("err: {}, cause: {}", err, cause))
                ;
    }


    @PostMapping("/lectures/{courseCode}")
    Flux<Lecture> scrapeLectures(@PathVariable String courseCode) {
        log.info("scrapeLectures() for: {}", courseCode);

        return courseRepository.findById(courseCode)    // get single course
                .flatMapMany(course -> scraperClient.scrapeTimetable(course.toProto()) // scrape all Lectures for that curse
                        .log()
                        .flatMap(protoLecture -> {  // for each lecture do
                            // save staff
                            var staff = Staff.from(protoLecture.getStaff());
                            var staffSave = staffRepository.save(staff)
                                    .onErrorResume(err -> {
                                        log.warn("Recovering from saving: {}", staff);
                                        return staffRepository.findOneByNameAndSurname(staff.getName(), staff.getSurname());
                                    })
                                    .doOnNext(module1 -> log.info("After Staff save: {}", module1))
                                    .doOnError((err) -> log.warn("Insert: {}, error: {}", staff, err.getMessage()))
//                                    .onErrorContinue((err, o) -> log.warn("Insert: {} error: {}", staff, err))
                                    ;

                            // save room
                            var room = Room.from(protoLecture.getRoom());
                            var roomSave = roomRepository.insert(room.getRoomNo(), room.getDescription())
                                    .onErrorResume((err) -> {
                                        log.warn("Recovering from insert: {}, error: {}", room, err.getMessage());
                                        return roomRepository.findById(room.getRoomNo());
                                    })
                                    .checkpoint()
                                    .doOnNext(module1 -> log.info("After Room save: {}", module1))
//                                    .doOnError((err) -> log.warn("Insert: {}, error: {}", room, err))
//                                    .flatMap(self -> roomRepository.findById(self.getRoomNo()))

//                                    .onErrorContinue((err, o) -> log.warn("Insert: {} error: {}", room, err))
                                    ;

                            // save module
                            var module = Module.from(protoLecture.getModule());
                            var moduleSave = moduleRepository.save(module)
                                    .onErrorResume(err ->{
                                        log.warn("Recovering from saving: {} with error: {}", module, err.getMessage());
                                        return moduleRepository.findOneByCourseCodeAndNameAndType(
                                                module.getCourseCode(), module.getName(), module.getType()
                                        );
                                    })
                                    .flatMap(m -> m.getId() > 0
                                            ? Mono.just(m)
                                            : moduleRepository.findOneByNotes(m.getNotes())
                                    )
                                    .log()
                                    .doOnNext(module1 -> log.info("After Module save: {}", module1))
//                                    .doOnError((err) -> log.warn("Insert: {}, error: {}", module, err))
//                                    .onErrorContinue((err, o) -> log.warn("Insert: {} error: {}", module, err))
                                    ;

                            // get ids
                            var lectureModuleId = moduleSave.map(Module::getId);
                            var lectureStaffId = staffSave.map(Staff::getId);

                            Hooks.onOperatorDebug();

                            // create and save lecture
//                            return roomSave.then(
//                                    lectureModuleId.zipWith(lectureStaffId)
                            return
//                                    lectureModuleId
                                    roomSave.zipWith(lectureModuleId, (self, other) ->  other )

                                            .zipWith(lectureStaffId, (modId, staffId)->

//                                            .map(tuple ->
                                                    Lecture.builder()
                                                    .moduleId(modId)// no module id
                                                    .roomNo(protoLecture.getRoom().getNumber())
                                                    .staffId(staffId)// no staff id
                                                    .weekday(protoLecture.getWeekDay().name())
                                                    .start(protoLecture.getStartTime())
                                                    .duration(protoLecture.getDuration())
                                                    .build()
                                            )
                                            .log()
                                            .checkpoint("Lecture before saving");

//                            )
//                            ;
                        })

//                                            .onErrorContinue((err, o) ->  log.warn("Lecture insert error: {} - {}", err, o))
                )
                .flatMap(lecture -> lectureRepository.insert(
                        lecture.getModuleId(), lecture.getRoomNo(), lecture.getStaffId(),
                        lecture.getWeekday(), lecture.getStart(), lecture.getDuration()
                        )
                        .onErrorResume(err -> {
                            log.warn("Lecture {} insert error: "+ err.getMessage(), lecture);
                            return lectureRepository.findByRoomNoAndWeekdayAndStart(
                                    lecture.getRoomNo(), lecture.getWeekday(), lecture.getStart()
                            );
                        })
                        .doOnError(err ->  log.warn("Problem inserting {} insert error: "+ err.getMessage(), lecture))
                )
                ;
        // TODO: notes: maybe repository find is caled after it was closed
    }


}
