package ie.gmit.timetable.controllers;

import ie.gmit.timetable.models.Department;
import ie.gmit.timetable.repositories.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/departments")
public class DepartmentController {

    @Qualifier("departmentRepository")
    private final DepartmentRepository repository;

    public DepartmentController(DepartmentRepository repository){
        this.repository = repository;
    }

    @GetMapping
    Flux<Department> getAll(){
        return this.repository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Mono<Void> create(@RequestBody Department personStream) {
        return this.repository.save(personStream).then();
    }

    @GetMapping("/{id}")
    Mono<Department> findById(@PathVariable long id) {
        return this.repository.findById(id);
    }

    @GetMapping("/name")
    Flux<Department> findByName(@RequestParam String query) {
        return this.repository.findByName(query);
    }

    @GetMapping("/type")
    Flux<Department> findByType(@RequestParam String query) {
        return this.repository.findByType(query);
    }

    @GetMapping("/campus")
    Flux<Department> findByCampus(@RequestParam String query) {
        return this.repository.findByCampus(query);
    }

//    @GetMapping("/{name}")
//    Flux<Department> findByName(@RequestBody Department department) {
//        log.info("GET /departments/name got: {}", department);
//        var name = department.getName();
//        return this.repository.findByName(name);
//    }
}
