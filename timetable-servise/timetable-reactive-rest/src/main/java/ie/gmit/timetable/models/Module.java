package ie.gmit.timetable.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("modules")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Module {
    @Id
    private int id;
    private String courseCode;
    private String name;
    private String type;
    private String weeks;
    private String notes;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    public static Module from(ie.gmit.proto.Module module) {
        return Module.builder()
                .courseCode("G-"+ module.getCourseCode()) // ToDo: this is temp fix.
                .name(module.getModuleName())
                .type(module.getType().name())
                .weeks(module.getWeeks())
                .notes(module.getNotes())
                .build()
                ;
    }
}
