package ie.gmit.timetable.conf;

import ie.gmit.timetable.repositories.DepartmentRepository;
import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Configuration
@ConfigurationProperties("spring.datasource")
public class PostgresConfig extends AbstractR2dbcConfiguration {
    private String url;
    @Builder.Default private int port = 5432;
    @Builder.Default private String host = "127.0.0.1";
    @Builder.Default private String database = "timetable_test";
    @Builder.Default private String username = "postgres";
    @Builder.Default private String password = "postgres";

    @Bean
    @Override
    public ConnectionFactory connectionFactory() {
        log.info("Creating bean for PostgreSQL database: {} on {}:{}", database, host, port);
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                        .port(port)
                        .host(host)
                        .database(database)
                        .username(username)
                        .password(password)
                        .build()
        );
//        return ConnectionFactories.get(
//                String.format("r2dbc:postgres://%s:5432/%s", host, database)
//        );

//    ConnectionFactory connectionFactory = ConnectionFactories.get("r2dbc:postgres://<host>:5432/<database>");
//    Publisher<? extends Connection> connectionPublisher = connectionFactory.create();
    }


}
