package ie.gmit.timetable.models;

import ie.gmit.proto.Lecturer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Slf4j
@Table("staff")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Staff {
    @Id
    private int id;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    public static Staff from(Lecturer staff) {
        return Staff.builder()
                .name(staff.getName())
                .surname(staff.getSurname())
                .email(staff.getEmail())
                .build()
                ;
    }
}
