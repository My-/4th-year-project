package ie.gmit.timetable.controllers;


import ie.gmit.timetable.models.*;
import ie.gmit.timetable.models.Module;
import ie.gmit.timetable.repositories.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Slf4j
@RestController
@RequestMapping("/storage")
public class StorageController {

    @Qualifier("courseRepository")
    private final CourseRepository courseRepository;

    @Qualifier("departmentRepository")
    private final DepartmentRepository departmentRepository;

    @Qualifier("lectureRepository")
    private final LectureRepository lectureRepository;

    @Qualifier("departmentRepository")
    private final ModuleRepository moduleRepository;

    @Qualifier("departmentRepository")
    private final RoomRepository roomRepository;

    @Qualifier("departmentRepository")
    private final StaffRepository staffRepository;

    public StorageController(CourseRepository courseRepository,
                             DepartmentRepository departmentRepository,
                             LectureRepository lectureRepository,
                             ModuleRepository moduleRepository,
                             RoomRepository roomRepository,
                             StaffRepository staffRepository
    ) {
        this.courseRepository = courseRepository;
        this.departmentRepository = departmentRepository;
        this.lectureRepository = lectureRepository;
        this.moduleRepository = moduleRepository;
        this.roomRepository = roomRepository;
        this.staffRepository = staffRepository;
    }

    @PostMapping("/departments")
    Flux<Department> insertDepartments(@RequestBody List<ie.gmit.Department> departments) {
        log.info("Adding to storage: ({}) {}", departments.size(), departments);
        return Flux.fromIterable(departments)
                .map(Department::from)
                .flatMap(departmentRepository::save)
                ;
    }

    @PostMapping("/courses")
    Flux<Course> insertCourses(@RequestBody List<ie.gmit.Course> courses) {
        log.info("Adding to storage: ({}) {}", courses.size(), courses);
        return Flux.fromIterable(courses)
                .filter(inCourse -> {
                    if (Objects.isNull(inCourse.getDepartment())) {
                        log.warn("Department is null: {}", inCourse);
                        return false;
                    }

                    return true;
                })
                .flatMap(inCourse -> departmentRepository
                        .findOneByCampusAndNameAndType(
                                inCourse.getDepartment().getName(),
                                inCourse.getDepartment().getType().name(),
                                inCourse.getDepartment().getCampus().name()
                        )
                        .map(Department::getId)
                        .map(depId -> Course.builder()
                                .name(inCourse.getName())
                                .notes(inCourse.getNotes())
                                .courseCode(inCourse.getCourseCode())
                                .semester(inCourse.getSemester())
                                .departmentId(depId)
                                .build()
                        )
                )
                .doOnNext(it -> log.info("it: {}", it))
                .flatMap(dep -> courseRepository.insert(
                        dep.getCourseCode(),
                        dep.getDepartmentId(),
                        dep.getName(),
                        dep.getYear(),
                        dep.getSemester(),
                        dep.getNotes())
                )
                ;
    }

    @PostMapping("/rooms")
    Flux<Room> insertRooms(@RequestBody List<Room> rooms) {
        log.info("Adding to storage: ({}) {}", rooms.size(), rooms);
        return Flux.fromIterable(rooms)
                .flatMap(room -> roomRepository.insert(room.getRoomNo(), room.getDescription()))
                ;
    }

    @PostMapping("/modules")
    Flux<Module> insertModules(@RequestBody List<Module> modules) {
        log.info("Adding to storage: ({}) {}", modules.size(), modules);
        return moduleRepository.saveAll(modules)
                ;
    }


    @PostMapping("/staff")
    Flux<Staff> insertLecturor(@RequestBody List<Staff> staff) {
        log.info("Adding to storage: ({}) {}", staff.size(), staff);
        return staffRepository.saveAll(staff)
                ;
    }



    @PostMapping("/lectures")
    Flux<Lecture> insertLecture(@RequestBody List<Lecture> lectures) {
        log.info("Adding to storage: ({}) {}", lectures.size(), lectures);
        return
                Flux.fromIterable(lectures)
                .flatMap(lectureRepository::save)

//                lectureRepository.saveAll(lectures)
                ;
    }


}
