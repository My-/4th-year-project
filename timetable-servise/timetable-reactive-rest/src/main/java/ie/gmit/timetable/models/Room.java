package ie.gmit.timetable.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("rooms")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Room {
    @Id
    private int roomNo;
    private String description;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    public static Room from(ie.gmit.proto.Room room) {
        return Room.builder()
                .roomNo(room.getNumber())
                .description(room.getDescription())
                .build()
                ;
    }
}
