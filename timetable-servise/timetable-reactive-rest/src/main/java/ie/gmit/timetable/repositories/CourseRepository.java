package ie.gmit.timetable.repositories;

import ie.gmit.timetable.models.Course;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface CourseRepository extends ReactiveCrudRepository<Course, String> {
    @Query("SELECT * FROM courses WHERE department_id IN(:departmentId)")
    Flux<Course> findAllByDepartmentId(int departmentId);

    @Query("INSERT INTO courses(course_code, department_id, name, year, semester, notes)\n" +
            "VALUES (:courseCode, :departmentId, :name, :year, :semester, :notes)")
    Flux<Course> insert(String courseCode, int departmentId, String name, int year, int semester, String notes);
}
