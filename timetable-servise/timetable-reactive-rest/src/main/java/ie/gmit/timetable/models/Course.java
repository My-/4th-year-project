package ie.gmit.timetable.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("courses")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Course {
    @Id
    private String courseCode;
    private int departmentId;
    private String name;
    private int year;
    private int semester;
    private String notes;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    public static Course from(ie.gmit.proto.Course course) {
        return Course.builder()
                .courseCode(course.getCourseCode())
                .name(course.getName())
                .year(course.getYear())
                .semester(course.getSemester())
                .notes(course.getNotes())
                .build()
                ;
    }

    public boolean isFirstTerm(){
        return semester % 2 == 1;
    }

    public boolean isSecondTerm(){
        return semester % 2 == 0;
    }

    public ie.gmit.proto.Course toProto() {
        return ie.gmit.proto.Course.newBuilder()
                .setCourseCode(courseCode)
                .setName(name)
                .setYear(year)
                .setSemester(semester)
                .setNotes(notes)
                .build()
                ;
    }
}
