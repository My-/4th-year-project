package ie.gmit.timetable.repositories;

import ie.gmit.timetable.models.Lecture;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface LectureRepository  extends ReactiveCrudRepository<Lecture, String> {
    @Query("INSERT INTO lectures(module_id, room_no, staff_id, weekday, start, duration) " +
            "VALUES (:module_id, :room_no, :staff_id, :weekday, :start, :duration)")
    Flux<Lecture> insert(int module_id, int room_no, int staff_id, String weekday, int start, int duration);

    @Query("SELECT * FROM lectures WHERE room_no IN(:roomNo) AND weekday IN(:weekday) AND start IN(:start)")
    Mono<Lecture> findByRoomNoAndWeekdayAndStart(int roomNo, String weekday, int start);
}
