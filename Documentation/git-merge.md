# How to merge one git project in to another

While ding project I had a need to merge two projects in to one, but I wanted to keep commits of thouse projects. Git has handy comand `subtree` ([doc](https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt)).
```bash
git subtree add -P <prefix> <repo> <rev>
```
For example: *you have your `main-project` and you want to add `side-project` to it with all its commits. You could (I asume bouth projects uses git):
```bash
# go to your `main-roject` directory
git subtree add -P My-Side-Project-Dir-Name-Inside-Main-Project /path/to/side-project/ side-project-branch
```
As you can guess `side-project-branch` from `side-project` will be copied to the `main-project` current branch and all code placed to the `My-Side-Project-Dir-Name-Inside-Main-Projec` directory.
