# Minimizing Rust binaries

> Ref: https://github.com/johnthagen/min-sized-rust

```bash
# build for realease
cargo build --release

strip target/release/min-sized-rust
```
