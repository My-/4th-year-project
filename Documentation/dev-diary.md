# Developer diary

### week 1
Researched available options for the web scraping.  
[issue](https://gitlab.com/My-/4th-year-project/issues/12)
Start building scraper.  

### week 2 (start of January)
Coded scraper using Selenium and JS.  
Created POC grpc between Java and JS.  

### week 3
Continue to work on gRPC between JS and Java.  
Researched Postgres DB as an option for tt storage  

### week 4
Finished scraper missing parts and fixed many issues.  
Implemented Scraper 3 main method to use gRPC.  
Tested PostgeSQL with Reactive driver.
Tried SpringBoot for Rest but [issue](https://gitlab.com/My-/4th-year-project/issues/26)  

### week 5
Created workaround for my [SpringBoot issue](https://stackoverflow.com/a/59957606/5322506)  
Got idea to create codegen as solution is wery repeditive.  
Started code it in Rust.  
Created two java codegens:
- for wrapers
- for interfaces  
(It took almost full week as parse `.proto` files was more chalanging then I tought it would, doesnt feel was worth it)
Created separate reusable `.jar` containing models. It should help with code refactoring.

### week 6 (start of February)
> JH told us what code should be done in 2 max 4 weeks :( as wee need to start write desertation wrom this date

Started to work on `storage service`.  
Designed and created Postgres DB scema.  
Tested `Vert.x` diver. Created POC.  
Decided to swich to `R2DBC` as it uses `Reactor`, same as Spring boot.  
Had hard time to use Postgres enum type. [issue](https://gitlab.com/My-/4th-year-project/issues/34)  


### week 7 (February week 2)
Created simple POC storage service, but later decided to use SpringBoot feature which allows to switch db's as needed. I didn't knew SpringBoot had that feature.  
Started implement Reactive REST for inserting data to DB.  


### week 8
Reactive REST for inserting data is almost done.  
Merged all branches to `master`.  
Fixed time table screper (grpc now works).    
Learned how to use configuration files in SpringBoot.  
Start working on other projects.  


### week 9
Doing other projects.  
Started integrating scraper client into rest server.  
Integrated 2/3 scraper functionalities.  


### week 10
Started integrate last scraper cielnt grps methon into rest server.  
Fixed at least 20 all kind of problems. This [issue](https://gitlab.com/My-/4th-year-project/-/issues/49) has some of them I had.  


### week 11

### week 12

### week 13
The End
