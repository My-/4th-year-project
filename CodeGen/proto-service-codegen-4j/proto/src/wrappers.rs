use std::fmt::{Display, Formatter, Error};

#[derive(Debug)]
pub enum Wrapper {
    Unknown(String),
    DoubleValue,
    FloatValue,
    Int64Value,
    UInt64Value,
    Int32Value,
    UInt32Value,
    BoolValue,
    StringValue,
    BytesValue,
    Empty,
}

impl Wrapper {
    /// Get Wrapper from string
    pub fn from(value: &str) -> Wrapper {
        match value.trim() {
            "google.protobuf.DoubleValue" => Wrapper::DoubleValue,
            "google.protobuf.FloatValue" => Wrapper::FloatValue,
            "google.protobuf.Int64Value" => Wrapper::Int64Value,
            "google.protobuf.UInt64Value" => Wrapper::UInt64Value,
            "google.protobuf.Int32Value" => Wrapper::Int32Value,
            "google.protobuf.UInt32Value" => Wrapper::UInt32Value,
            "google.protobuf.BoolValue" => Wrapper::BoolValue,
            "google.protobuf.StringValue" => Wrapper::StringValue,
            "google.protobuf.BytesValue" => Wrapper::BytesValue,
            "google.protobuf.Empty" => Wrapper::Empty,
            _ => Wrapper::Unknown(String::from(value)),
        }
    }

/// Ref:
///     - https://github.com/protocolbuffers/protobuf/blob/master/src/google/protobuf/wrappers.proto
///     - https://developers.google.com/protocol-buffers/docs/proto3#scalar
    pub fn get_java_type(&self) -> &str {
        match self {
            self::Wrapper::Unknown(t) => t,
            self::Wrapper::DoubleValue => "double",
            self::Wrapper::FloatValue => "float",
            self::Wrapper::Int64Value => "long",
            self::Wrapper::UInt64Value => "long",
            self::Wrapper::Int32Value => "int",
            self::Wrapper::UInt32Value => "int",
            self::Wrapper::BoolValue => "boolean",
            self::Wrapper::StringValue => "String",
            self::Wrapper::BytesValue => "ByteString",
            self::Wrapper::Empty => "void",
        }
    }
}

impl Display for Wrapper {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match self {
            self::Wrapper::Unknown(s) => write!(f, "{}", s),
            self::Wrapper::DoubleValue => write!(f, "{}", "google.protobuf.DoubleValue"),
            self::Wrapper::FloatValue => write!(f, "{}", "google.protobuf.FloatValue"),
            self::Wrapper::Int64Value => write!(f, "{}", "google.protobuf.Int64Value"),
            self::Wrapper::UInt64Value => write!(f, "{}", "google.protobuf.UInt64Value"),
            self::Wrapper::Int32Value => write!(f, "{}", "google.protobuf.Int32Value"),
            self::Wrapper::UInt32Value => write!(f, "{}", "google.protobuf.UInt32Value"),
            self::Wrapper::BoolValue => write!(f, "{}", "google.protobuf.BoolValue"),
            self::Wrapper::StringValue => write!(f, "{}", "google.protobuf.StringValue"),
            self::Wrapper::BytesValue => write!(f, "{}", "google.protobuf.BytesValue"),
            self::Wrapper::Empty => write!(f, "{}", "google.protobuf.Empty"),
        };

        Ok(())
    }
}

