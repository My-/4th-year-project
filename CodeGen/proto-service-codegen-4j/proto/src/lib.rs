mod service;
mod rpc;
mod doc;
mod message;
mod wrappers;
mod proto_buff;

pub use crate::rpc::RPC;
pub use crate::service::Service;
pub use crate::doc::Doc;
pub use crate::message::Message;
pub use crate::wrappers::Wrapper;
pub use crate::proto_buff::ProtoBufType;
use std::fmt::{Display, Formatter, Error};

#[derive(Default, Debug)]
pub struct Proto {
    syntax: String,
    package: String,
    options: Vec<String>,
    imports: Vec<String>,
    services: Vec<Service>,
}

impl Proto {
    pub fn get_java_package(&self) -> &str {
        let option = self.options.iter()
            .find(|option| option.contains("java_package"))
//            .map(|e| e.matches(r"[a]").collect())
            .unwrap() // ToDo: fallback if java_package not provided
            ;

        let start = option.find("\"")
            .expect("can't find starting quote (\")");
        let end = option.rfind("\"")
            .expect("can't find end quote (\")");

        let option = &option[start+1..end];

//        println!("{:#?}", option);

        option
    }
    // Setters
    pub fn syntax(&mut self, syntax: &str) -> &mut Self {
        self.syntax = String::from(syntax);
        self
    }

    pub fn package(&mut self, package: &str) -> &mut Self {
        self.package = String::from(package);
        self
    }

    pub fn option(&mut self, option: &str) -> &mut Self {
        self.options.push(String::from(option));
        self
    }

    pub fn import(&mut self, import: &str) -> &mut Self {
        self.imports.push(String::from(import));
        self
    }

    pub fn service(&mut self, service: Service) -> &mut Self {
        self.services.push(service);
        self
    }

    // Getters
    pub fn get_syntax(&self) -> &str {
        &self.syntax
    }

    pub fn get_package(&self) -> &str {
        &self.package
    }

    pub fn get_options(&self) -> &[String] {
        &self.options
    }

    pub fn get_imports(&self) -> &[String] {
        &self.imports
    }

    pub fn get_services(&self) -> &[Service] {
        &self.services
    }
}

impl Display for Proto {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        writeln!(f, "{}", self.syntax)?;
        writeln!(f, "{}", self.package)?;
        self.options.iter()
            .for_each(|option|{
                writeln!(f, "{}", option);
            });
        self.imports.iter()
            .for_each(|import|{
                writeln!(f, "{}", import);
            });
        self.services.iter()
            .for_each(|service| {
                writeln!(f, "{}", service);
            });

        Ok(())
    }
}



#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);


    }
}
