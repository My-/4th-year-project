use std::fmt::{Display, Formatter, Error};

#[derive(Debug, Default)]
pub struct Doc {
    tabs: u8,
    text: Vec<String>,
}

impl Doc {
    pub fn set_tabs(&mut self, tabs: u8) -> &Self {
        self.tabs = tabs;
        self
    }

    pub fn text(&mut self, text: &str) -> &mut Self {
        self.text.push(String::from(text));
        self
    }
}

impl Display for Doc {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        writeln!(f, "/**")?;
        for l in &self.text {
            writeln!(f, "*\t{}", l)?;
        }
        write!(f, "\t*/")?;

        Ok(())
    }
}