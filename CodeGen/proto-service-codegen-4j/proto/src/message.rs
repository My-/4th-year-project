use std::fmt::{Display, Formatter};
use core::fmt;

type EnumValue = (String, u8);

#[derive(Clone, Debug, Eq, PartialEq, Hash, Default)]
pub struct Enum {
    name: String,
    values: Vec<EnumValue>
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum FieldType {
    Standard(String),
    Enum(String),
    Custom(String),
}

impl Display for FieldType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            FieldType::Standard(v) => write!(f,"{}", v),
            FieldType::Enum(v) => write!(f,"{}", v),
            FieldType::Custom(v) => write!(f,"{}", v),
        }
    }
}

#[derive(Default, Clone, Debug)]
pub struct Message {
    package: String,
    name: String,
    fields: Vec<(FieldType, String)>, // variable type - name
}

impl Message {
    pub fn name(&mut self, name: &str) -> &mut Self {
        self.name = String::from(name);
        self
    }
    pub fn get_name(&self) -> &str {
        &self.name
    }
}

impl Display for Message {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let proto_class_name = format!("{}.{}", self.package, self.name);
        let proto_var = format!("{}Proto", self.name.to_lowercase());
        let capitalize = |s:&str| format!("{}{}", s.get(0..1).unwrap().to_uppercase(), s.get(1..).unwrap());

        // comment
        writeln!(f, "// This is generated class")?;
        // package
        write!(f, "package {};\n\n", self.package.replace(".proto", ""))?;
        // lombok dependency
        writeln!(f, "import lombok.Data;\n\n@Data")?;
        // class
        writeln!(f, "public class {} {}", self.name, "{")?;
        // fields generator
        self.fields.iter()
            .for_each(|field| {
                let field_type = match &field.0 {
                    FieldType::Enum(v) => format!("{}.{}", self.package, v),
                    FieldType::Custom(v) => String::from(v),
                    FieldType::Standard(v) => String::from(v),
                };

                writeln!(f, "\tprivate {} {};", field_type, field.1).unwrap();
            });
        // constructor generator
        writeln!(f, "\n\tprivate {}({} {}) {3}", self.name, proto_class_name, proto_var, "{", )?;
        self.fields.iter()
            .for_each(|field| {
                let getter = format!("{}.get{}()", proto_var, capitalize(&field.1));
                let vv = match &field.0 {
                    FieldType::Enum(v) => String::from(&getter),
                    FieldType::Custom(v) => format!("{}.of({})", field.0, &getter),
                    FieldType::Standard(v) => String::from(&getter),
                };
                writeln!(f, "\t\tthis.{} = {};", field.1, vv);
            });
        writeln!(f, "\t{}", "}\n")?;
        //factory method generator
        writeln!(f, "\tpublic static {} of({} {}) {}", self.name, proto_class_name, proto_var, "{")?;
        writeln!(f, "\t\treturn new {}({});", self.name, proto_var)?;
        writeln!(f, "\t{}", "}\n")?;
        // `toProto()` generator
        writeln!(f, "\tpublic {} toProto() {}", proto_class_name, "{")?;
        writeln!(f, "\t\treturn {}.newBuilder()", proto_class_name)?;
        self.fields.iter()
            .for_each(|field| {
                let to_proto_text = match &field.0 {
                    FieldType::Custom(v) => ".toProto()",
                    _ => "",
                };
                writeln!(f, "\t\t\t.set{}(this.{}{})", capitalize(&field.1), field.1, to_proto_text).unwrap();
            });
        writeln!(f, "\t\t\t.build();")?;
        writeln!(f, "\t{}", "}")?;
        writeln!(f, "{}", "}")?;

        Ok(())
    }
}