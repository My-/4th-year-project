use crate::{Doc, ProtoBufType};
use std::fmt::{Display, Formatter, Error};

#[derive(Default, Debug)]
pub struct RPC {
    name: String,
    takes: ProtoBufType,
    returns: ProtoBufType,
    doc: Doc,
}

impl RPC {
    pub fn name(&mut self, name: &str) -> &mut Self {
        self.name = String::from(name);
        self
    }
    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn takes(&mut self, takes: &str) -> &mut Self {
        self.takes = ProtoBufType::from(takes);
        self
    }
    pub fn get_takes(&self) -> &str {
        self.takes.get_java_type()
    }

    pub fn returns(&mut self, returns: &str) -> &mut Self {
        self.returns = ProtoBufType::from(returns);
        self
    }
    pub fn get_returns(&self) -> &str {
        self.returns.get_java_type()
    }

    pub fn doc(&mut self, doc: Doc) -> &mut Self {
        self.doc = doc;
        self
    }
    pub fn get_doc(&self) -> &Doc {
        &self.doc
    }
}

impl Display for RPC {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        writeln!(f, "\n\t{}", self.doc)?;
        let last_dot = self.takes.get_java_type().rfind('.')
            .map(|n| n + 1)
            .unwrap_or(0_usize);

        writeln!(f, "\tpublic {} {}({} {});",
                 self.returns.get_java_type(),
                 self.name,
                 if self.takes.get_java_type() == "void" { "" } else { self.takes.get_java_type() },
                 if &self.takes.get_java_type()[last_dot..].to_lowercase() == "void" { "".to_owned() }
                 else { self.takes.get_java_type()[last_dot..].to_lowercase() })?;

        Ok(())
    }
}