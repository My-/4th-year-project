use crate::Wrapper;
use std::fmt::{Debug, Display, Formatter, Error};

///  https://developers.google.com/protocol-buffers/docs/proto3#scalar
#[derive(Debug)]
pub enum ProtoBufType {
    Unknown(Wrapper),
    Double,
    Float,
    Int32,
    Int64,
    Uint32,
    Uint64,
    Sint32,
    Sint64,
    Fixed32,
    Fixed64,
    Sfixed32,
    Sfixed64,
    Bool,
    String,
    Bytes,
}

impl ProtoBufType {
    /// Create `ProtoBufType` from sting
    pub fn from(value: &str) -> ProtoBufType {
        match value.trim() {
            "double" => ProtoBufType::Double,
            "float" => ProtoBufType::Float,
            "int32" => ProtoBufType::Int32,
            "int64" => ProtoBufType::Int64,
            "uint32" => ProtoBufType::Uint32,
            "uint64" => ProtoBufType::Uint64,
            "sint32" => ProtoBufType::Sint32,
            "sint64" => ProtoBufType::Sint64,
            "fixed32" => ProtoBufType::Fixed32,
            "fixed64" => ProtoBufType::Fixed64,
            "sfixed32" => ProtoBufType::Sfixed32,
            "sfixed64" => ProtoBufType::Sfixed64,
            "bool" => ProtoBufType::Bool,
            "string" => ProtoBufType::String,
            "bytes" => ProtoBufType::Bytes,
            _ => ProtoBufType::Unknown(Wrapper::from(value.trim())),
        }
    }

    /// ref: https://developers.google.com/protocol-buffers/docs/proto3#scalar
    pub fn get_java_type(&self) -> &str {
        match self {
            self::ProtoBufType::Unknown(wrapper) => wrapper.get_java_type(),
            self::ProtoBufType::Double => "double",
            self::ProtoBufType::Float => "float",
            self::ProtoBufType::Int32 => "int",
            self::ProtoBufType::Int64 => "long",
            self::ProtoBufType::Uint32 => "int",
            self::ProtoBufType::Uint64 => "long",
            self::ProtoBufType::Sint32 => "int",
            self::ProtoBufType::Sint64 => "long",
            self::ProtoBufType::Fixed32 => "int",
            self::ProtoBufType::Fixed64 => "long",
            self::ProtoBufType::Sfixed32 => "int",
            self::ProtoBufType::Sfixed64 => "long",
            self::ProtoBufType::Bool => "boolean",
            self::ProtoBufType::String => "String",
            self::ProtoBufType::Bytes => "ByteString",
        }
    }

}

impl Default for ProtoBufType {
    fn default() -> Self {
        ProtoBufType::Unknown(Wrapper::from(""))
    }
}

impl Display for ProtoBufType {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match self {
            self::ProtoBufType::Unknown(wrapper) => write!(f, "{}", wrapper),
            self::ProtoBufType::Double => write!(f, "{}", "double"),
            self::ProtoBufType::Float => write!(f, "{}", "float"),
            self::ProtoBufType::Int32 => write!(f, "{}", "int32"),
            self::ProtoBufType::Int64 => write!(f, "{}", "int64"),
            self::ProtoBufType::Uint32 => write!(f, "{}", "uint32"),
            self::ProtoBufType::Uint64 => write!(f, "{}", "uint64"),
            self::ProtoBufType::Sint32 => write!(f, "{}", "sint32"),
            self::ProtoBufType::Sint64 => write!(f, "{}", "sint64"),
            self::ProtoBufType::Fixed32 => write!(f, "{}", "fixed32"),
            self::ProtoBufType::Fixed64 => write!(f, "{}", "fixed64"),
            self::ProtoBufType::Sfixed32 => write!(f, "{}", "sfixed32"),
            self::ProtoBufType::Sfixed64 => write!(f, "{}", "sfixed64"),
            self::ProtoBufType::Bool => write!(f, "{}", "bool"),
            self::ProtoBufType::String => write!(f, "{}", "string"),
            self::ProtoBufType::Bytes => write!(f, "{}", "bytes"),
        };

        Ok(())
    }
}