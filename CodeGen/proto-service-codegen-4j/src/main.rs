#[macro_use] extern crate clap;

use regex::Regex;
use std::fs;
use std::io;
use std::mem;
use std::io::BufRead;
use std::io::Write;
use proto;
use proto::Service;

fn main() {
    // Clap (cli) description
    let matches = clap_app!(myapp =>
        (version: "0.1")
        (author: "Mindaugas Sh. <g00339629@gmit.ie>")
        (about: "Generates Java interface from proto file.")
        (@arg INPUT: -i --infile +required +takes_value "Sets the proto file to use")
        (@arg OUT_DIR: -o --outdir +takes_value "Sets an output directory")
        (@arg verbose: -v --verbose "Sets the verbosity on")
    ).get_matches();

    // get cli values
    let is_verbose = matches.is_present("verbose");
    let in_file = matches.value_of("INPUT")
        .expect("Input file is needed!");
    let out_dir = matches.value_of("OUT_DIR");

    if !in_file.ends_with(".proto") {
        panic!("Provided file should be a protobuf file (.proto)")
    }


    // ref: https://stackoverflow.com/a/31193386/5322506
    // file read
    let f = fs::File::open(in_file)
        .expect(&format!("Unable to open file: {}", in_file));
    let f = io::BufReader::new(f);

    // mutable variables
    let mut proto_file = proto::Proto::default();
    let mut proto_service = proto::Service::default();
    let mut proto_rpc = proto::RPC::default();
    let mut proto_doc = proto::Doc::default();
    let mut proto_message = proto::Message::default();

    // regex
    let re_rpc = Regex::new(r"([()^ ])")
        .expect("Invalid regular expresion");

    // for each `.proto` line
    for line in f.lines() {
        let line = line.expect("Unable to read line");
        let line = line.trim();

        // skip empty lines
        if line.is_empty() { continue; }

        let keyword_end = line.find(" ").unwrap_or(line.len());
        let mut keyword = &line[..keyword_end];
//        println!("keyword: {}", keyword);

        // comments
        if keyword == "///" {
            proto_doc.text(&line[keyword_end..]);
            continue;
        }

        // messages
        if !proto_message.get_name().is_empty() || keyword == "message" {
            // we ignore message creation for now
            if proto_message.get_name().is_empty() {
                proto_message.name(line);
                continue;
            }

            if !line.starts_with("}") {
                continue;
            }

            mem::replace(&mut proto_message, proto::Message::default());

        }

        // rpc
        if keyword == "rpc" {
//            println!("{}", line);
            let data:Vec<_> = re_rpc.split(line)
                .filter(|s| !s.is_empty())
                .collect();

//            println!("{:#?}", data);
            proto_rpc.doc(
                mem::replace(&mut proto_doc, proto::Doc::default())
            );
            if data[0] != "rpc" { panic!("missing \"rpc\" in: {}", line); }
            proto_rpc.name(data[1]);
            proto_rpc.takes(data[2]);
            if data[3] != "returns" { panic!("missing \"returns\" in {}", line); }
            let is_list = data[4] == "stream";
            proto_rpc.returns(&format!("{}{}{}",
                if is_list { "List<" } else { "" },
                if is_list { data[5] } else { data[4] },
                if is_list { ">" } else { "" }
            ));
//            println!("{}", proto_rpc);
            if !line.ends_with("}") {
                continue;
            }
        }

        // service
        if !proto_service.get_name().is_empty() || keyword == "service" {
//            println!("{}", line);
            if proto_service.get_name().is_empty() {
                proto_service.name(line.split_whitespace().collect::<Vec<_>>()[1]);
                proto_service.doc(
                    mem::replace(&mut proto_doc, proto::Doc::default())
                );
                continue;
            }
            else if !line.starts_with("}") {
                proto_service.rpc(
                    mem::replace(&mut proto_rpc, proto::RPC::default())
                );
                continue;
            }
        }

//        println!("{}", proto_service);
        // end of service, write to `Proto`
        if !proto_service.get_name().is_empty() {
            proto_service.package(proto_file.get_java_package());
            proto_file.service(
                // https://stackoverflow.com/a/27098807/5322506
                mem::replace(&mut proto_service, Service::default())
            );
        }

//        println!("Proto: {}",  line);
        // proto file data
        match keyword {
            "syntax" => proto_file.syntax(&line[keyword_end..]),
            "option" => proto_file.option(&line[keyword_end..]),
            "package" => proto_file.package(&line[keyword_end..]),
            "import" => proto_file.import(&line[keyword_end..]),
//            "service" => proto_file.service(
//                // https://stackoverflow.com/a/27098807/5322506
//                mem::replace(&mut proto_service, Service::default())
//            ),
            _ => &proto_file,
        };

//        if line.starts_with("}") && !proto_file.get_package().is_empty() {
//            println!("{}", proto_file);
//        }
    }

    // file write
    if let Some(o) = matches.value_of("OUT_DIR") {
//        println!("Value for output: {}", o);
        let dir = proto_file.get_java_package();
        let dir:Vec<&str> = dir.split(".").collect();
        let mut path = String::from(o);
        for d in dir {
            path.push_str(&format!("/{}", d));
            if fs::create_dir(&path).is_err() && is_verbose{
                println!("{}", format!("Already exist: {}", path));
            }
        }
        // write classes to created directory
        for service in proto_file.get_services() {
            if is_verbose {
                println!("{}", service);
            }
            let file = &format!("{}/{}Service.java", path, service.get_name());
            let f = fs::File::create(file)
                .expect(&format!("Unable to create file: {}", file));
            let mut f = io::BufWriter::new(f);
            write!(f, "{}", service).expect("Unable to write data");
        }
    }


}
