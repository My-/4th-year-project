extern crate clap;

use clap::{Arg, App};

use std::error::Error;
use std::fmt::{Display, Formatter, Debug};
use std::{fmt};
use std::fs::{File, create_dir};
use std::io::{BufRead, BufReader, BufWriter};
use std::io::Write as IoWrite;
use std::hash::Hash;
use std::collections::HashSet;


type EnumValue = (String, u8);

#[derive(Clone, Debug, Eq, PartialEq, Hash, Default)]
struct Enum {
    name: String,
    values: Vec<EnumValue>
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum FieldType {
    Standard(String),
    Enum(String),
    Custom(String),
}

impl Display for FieldType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            FieldType::Standard(v) => write!(f,"{}", v),
            FieldType::Enum(v) => write!(f,"{}", v),
            FieldType::Custom(v) => write!(f,"{}", v),
        }
    }
}

#[derive(Default, Clone, Debug)]
struct Message {
    package: String,
    name: String,
    fields: Vec<(FieldType, String)>, // variable type - name
}

impl Display for Message {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let proto_class_name = format!("{}.{}", self.package, self.name);
        let proto_var = format!("{}Proto", self.name.to_lowercase());
        let capitalize = |s:&str| format!("{}{}", s.get(0..1).unwrap().to_uppercase(), s.get(1..).unwrap());

        // comment
        writeln!(f, "// This is generated class")?;
        // package
        write!(f, "package {};\n\n", self.package.replace(".proto", ""))?;
        // lombok dependency
        writeln!(f, "import lombok.Data;\n\n@Data")?;
        // class
        writeln!(f, "public class {} {}", self.name, "{")?;
        // fields generator
        self.fields.iter()
            .for_each(|field| {
                let field_type = match &field.0 {
                    FieldType::Enum(v) => format!("{}.{}", self.package, v),
                    FieldType::Custom(v) => String::from(v),
                    FieldType::Standard(v) => String::from(v),
                };

                writeln!(f, "\tprivate {} {};", field_type, field.1).unwrap();
            });
        // constructor generator
        writeln!(f, "\n\tprivate {}({} {}) {3}", self.name, proto_class_name, proto_var, "{", )?;
        self.fields.iter()
            .for_each(|field| {
                let getter = format!("{}.get{}()", proto_var, capitalize(&field.1));
                let vv = match &field.0 {
                    FieldType::Enum(v) => String::from(&getter),
                    FieldType::Custom(v) => format!("{}.of({})", field.0, &getter),
                    FieldType::Standard(v) => String::from(&getter),
                };
                writeln!(f, "\t\tthis.{} = {};", field.1, vv);
            });
        writeln!(f, "\t{}", "}\n")?;
        //factory method generator
        writeln!(f, "\tpublic static {} of({} {}) {}", self.name, proto_class_name, proto_var, "{")?;
        writeln!(f, "\t\treturn new {}({});", self.name, proto_var)?;
        writeln!(f, "\t{}", "}\n")?;
        // `toProto()` generator
        writeln!(f, "\tpublic {} toProto() {}", proto_class_name, "{")?;
        writeln!(f, "\t\treturn {}.newBuilder()", proto_class_name)?;
        self.fields.iter()
            .for_each(|field| {
                let to_proto_text = match &field.0 {
                    FieldType::Custom(v) => ".toProto()",
                    _ => "",
                };
                writeln!(f, "\t\t\t.set{}(this.{}{})", capitalize(&field.1), field.1, to_proto_text).unwrap();
            });
        writeln!(f, "\t\t\t.build();")?;
        writeln!(f, "\t{}", "}")?;
        writeln!(f, "{}", "}")?;

        Ok(())
    }
}



fn main() -> Result<(), Box<dyn Error>>
{
    // ref: https://github.com/clap-rs/clap/blob/master/examples/01b_quick_example.rs

    let matches = App::new("Wrapper for protobuf generated Java classes")
        .version("0.1")
        .author("Mindaugas Sharskus <gg00339629@gmit.ie>")
        .about("Crates Java code (Wraps generated protobuf Java classes)")
        .arg(Arg::with_name("INPUT")
            .help("Sets the input file to use")
            .required(true)
            .index(1)
        )
        .arg(
            Arg::with_name("OUTPUT")
                .short("o")
                .long("output")
                .value_name("OUT_DIR")
                .help("Sets an output directory")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("debug")
                .short("d")
                .long("debug")
                .multiple(true)
                .help("Turn debugging information on"),
        )
        .subcommand(
            App::new("test")
                .about("does testing things")
                .arg(Arg::with_name("list").short("l").help("lists test values")),
        )
        .get_matches();

    // You can check the value provided by positional arguments, or option arguments
    let i = matches.value_of("INPUT");
    let i = i.expect("no input message");
//    println!("Value for input: {}", i);


//    if let Some(o) = matches.value_of("OUTPUT") {
//        println!("Value for output: {}", o);
//    }
//
//    if let Some(c) = matches.value_of("debug") {
//        println!("Value for debug: {}", c);
//    }
//
//    // You can see how many times a particular flag or argument occurred
//    // Note, only flags can have multiple occurrences
//    match matches.occurrences_of("debug") {
//        0 => println!("Debug mode is off"),
//        1 => println!("Debug mode is kind of on"),
//        2 => println!("Debug mode is on"),
//        3 | _ => println!("Don't be crazy"),
//    }

    let mut is_debug = false;
    if let Some(_c) = matches.value_of("debug") {
        println!("Debug is on");
        is_debug = true;
    }


    // ref: https://stackoverflow.com/a/31193386/5322506
    // file read
    let f = File::open(i).expect("Unable to open file");
    let f = BufReader::new(f);

    let mut is_enum = false;
    let mut messages:Vec<Message> = Vec::new();
    let mut message = Message::default();
    let mut enum_set:HashSet<Enum> = HashSet::new();

    for line in f.lines() {
        let line = line.expect("Unable to read line");
        let line = line.replace(";", "");
        let line = line.trim();
        // empty or comment `//`
        if line.is_empty() || line.starts_with("//"){
            continue;
        }

        // get rid of enums
        if is_enum || line.starts_with("enum"){
            if line.starts_with("enum") {
                let mut enumas = Enum::default();
                let line:Vec<&str> = line.split(" ").collect();
                enumas.name = line[1].to_owned();
                enum_set.insert(enumas.clone());
                is_enum = true;
                // enum inside message
                if !message.name.is_empty() {
                    for v in &mut message.fields {
                        if v.0 == FieldType::Custom(enumas.name.clone()) {
                            let tmp = FieldType::Enum(format!("{}.{}", message.name, enumas.name));
                            v.0 = tmp;
                        }
                    }
                }
            }

            if is_enum && line.ends_with("}"){
                is_enum = false;
            }
            continue;
        }

        // syntax
        if line.starts_with("syntax"){
            continue;
        }

        // option
        if line.starts_with("option") {
            // java_package
            if line.contains("java_package") {
                let st_pos = line.find("\"").unwrap() +1;
                let end_pos = line.rfind("\"").unwrap();

                message.package.push_str(&line[st_pos .. end_pos]);
            }
            continue;
        }

        // protobuf package
        if line.starts_with("package") {
            if message.package.is_empty() {
                let line = line.replace("package", "");
                message.package.push_str(line.trim());
            }
            continue;
        }

        // message name
        if line.contains("message") {
            let line = line.split(" ").collect::<Vec<&str>>();
            message.name = line[1].trim().to_owned();
            continue;
        }

        // message data
        if !message.name.is_empty() {
            // message ends
            if line.rfind("}").is_some() {
                messages.push(message.clone());
                message.name.clear();
                message.fields = Vec::new();
            }
            else{
                // for each field
                let line = line.split(" ").collect::<Vec<&str>>();
                let field = match line[0] {
                    "string" => (FieldType::Standard(String::from("String")), String::from(line[1])),
                    "uint32" => (FieldType::Standard(String::from("int")), line[1].to_owned()),
                    _ => {
                        let mut tmp_enum = Enum::default();
                        tmp_enum.name = String::from(line[0]);
                        let mut val = (FieldType::Custom(line[0].to_owned()), line[1].to_owned());
                        if enum_set.contains(&tmp_enum) {
                            val = (FieldType::Enum(line[0].to_owned()), line[1].to_owned());
                        }
                        val
                    }, // ToDo: add enum check
                };
                message.fields.push(field)
            }
            continue;
        }
//        println!("{}", line);
    }

    // file write
    if let Some(o) = matches.value_of("OUTPUT") {
//        println!("Value for output: {}", o);
        let dir = message.package.replace(".proto", "");
        let dir:Vec<&str> = dir.split(".").collect();
        let mut path = String::from(o);
        for d in dir {
            path.push_str(&format!("/{}", d));
            if create_dir(&path).is_err() && is_debug{
                println!("{}", format!("Already exist: {}", path));
            }
        }
        // write classes to created directory
        for msg in messages {
            if is_debug {
                println!("{}", msg);
            }
            let file = &format!("{}/{}.java", path, msg.name);
            let f = File::create(file).expect(&format!("Unable to create file: {}", file));
            let mut f = BufWriter::new(f);
            write!(f, "{}", msg).expect("Unable to write data");
        }
    }

    Ok(())
}
