# Timetable API
 This is repository for the Timetable API I tried to create for my final year project. Plan was to take simple idea and  implement it using some new technologies I didnt use before and learn them on the way. 
 
### Goals
Here was three main goals for this project:
 - Build Timetable API what others could use.
 - Learn new things on the way.
 - get "real life experience".

### Project stack
 For this project I used:
 - JS, Selenium, gRPC - to create Timetable Scraper Service (TSS).
 - Reactive Java, Reactor, SpringBoot, gRPC, R2DBC - to create fully reactive Timetable Data Service (TDS)
 - PostgreSQL - for the storage.

 ### Project structure
 Project has two main services:
 - **TSS** or Timetable Scraper Service which is responsible for scraping colage timetable website for timetable data.
 - **TDS** or Timetable Data Service which is responsible for:
    - controling TSS *(almost all done)*,
    - storing scraped data to PostgreSQL database *(works)*,
    - allow data modifications *(not fully implemented)*,
    - allow data retrieve using:
        - REST *(little done)*,
        - gRPC *(not done)*,
        - RSocket *(not done)*,
        - GraphQL *(not done)*.

 **Note:** Some project parts are unfinished becaouse of COVID-19 lock down Collage timetable data was not available anymore and Ractive Java was taking more time then was expected.
 
 ##### Project diagram:

 ![Project diagram](./Documentation/project-diagram.jpg)

### Repository
This repository contains:
- `CodeGen` - contains two similar projects for Java code generation written in Rust language. Those generators was helping to crate Java code on any change in `proto` file.
- `Documentation` - is where project documentation should live. 
- `protos` - contains single `proto` file which should be use for data retrieval via `gRPC` from TDS.
- `timetable-service` - contais project code:
    - `JS_TimetableScraperSelenium` - contains separate project for TSS writen in JS *(fully finished)*.
    - `JavaTimetableClient` - 
    - `protos` - contains `proto` files for the TSS and TDS comunication via gRPC.
    - `timetable-models` - is a separate project containing DTO's (Data Transfer Objects) for the TDS.
    - `timetable-reactive-rest` - is a separate project which is *mutable part* of the TDS. This part responsible for controling TSS, for storing scraped data to PostgreSQL database and for modifying existing data. This project is using `timetable-scraper` and `timetable-models` internaly as dependencies.
    - `timetable-rest` - contains separate project for TDS REST timetabel data retrieval *(stubs are done, some logic is done, but mostly unfinish, early stage of the project)*.
    - `timetable-scraper` - contains separate Java project for the `gRPC` client to control the TSS *(fully finished)*.
    - `timetable-storage` - contains separate Java project for the storage abstraction. It should allow to swap database with ease. *But it is in very early stage.*

**Note:** most unfinished patrs is due COVID-19 and time constrains. During Project developmentI had few issues which took long time to solve. Is why here are more then one unfinishe project as while I was really stuck for solution I started other project parts.



### More Info
Here some stats about the project:
- [Repository stats][1] - language used and they code procentage, commit stats.
- [Repository issue graph][2] - issues open/closed one each month.
- [Repository commit graph][3] - visuavl presentation of all comits in the repository.
- [Contributors][4] - looks like I did split in to two doring project development :/
- [GitLab boards][5] - project planing and progres tracking.
- [Milestones][6] 
- [Project Overview Video][7]

This project is covered in detail in my disertation/report.

[1]: https://gitlab.com/My-/4th-year-project/-/graphs/master/charts
[2]: https://gitlab.com/My-/4th-year-project/insights/#/issues
[3]: https://gitlab.com/My-/4th-year-project/-/network/master
[4]: https://gitlab.com/My-/4th-year-project/-/graphs/master
[5]: https://gitlab.com/My-/4th-year-project/-/boards
[6]: https://gitlab.com/My-/4th-year-project/-/milestones
[7]: https://vimeo.com/417002342

